<?php

App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class IndexController extends AppController
{
    public $uses = array();

    public $components = array(
        'Session',
        'Cookie',
    );

    public $layout = "default";

    public $pass = "123";
    public $login = "admin";

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->layout = false;
        $this->set('title', "TEST WORK");
        $this->set('keywords', "TEST WORK");
        echo "TEST WORK";

    }

    public function auth_form()
    {    }

    public function whoami(){
        if($this->isValidToken()){
            echo "user authorized by " . $this->login;
            $this->response->header(array(
                //'Content-type: application/json',
                "Auth: user authorized by " . $this->login
            ));
            $this->response->statusCode(200);
            return $this->response;
        }
         else {
             echo "token is invalid";
             $this->response->header(array(
                 "Auth: token is invalid"
             ));
             $this->response->statusCode(401);
             return $this->response;
         }
    }

    /**
     * @param $login
     * @param $pass
     * @return bool
     */
    private function checkCredentialData($login, $pass)
    {
        if ($login == $this->login && $pass == $this->pass) {
            return true;
        }
        return false;
    }

    /**
     * @param $login
     * @param $pass
     * @return string
     */
    private function generateToken($login, $pass)
    {
        return hash('sha256', $login . uniqid() . $pass);
    }

    /**
     * @return bool
     */
    private function isValidToken(){
        $bearer = $this->getBearerToken();
        if($this->Cookie->read('bearer') == $bearer && $this->Cookie->check('bearer')!==false){
            return true;
        }
        return false;
    }

    /**
     *
     */
    public function auth()
    {
        $this->Cookie->write('bearer', null);
        $login = $this->request->data('login') ?? null;
        $password = $this->request->data('pass') ?? null;
        if ($login == null || $password == null){
            echo "WRONG LOGIN OR PASSWORD";
            $this->response->header(array(
                //'Content-type: application/json',
                "Auth: Failure"
            ));
            $this->response->statusCode(401);
            return $this->response;
        }
        if ($this->checkCredentialData($login, $password)) {
            $bearer = $this->generateToken($login, $password);

            //удачная авторизация
            $this->Cookie->write('bearer', $bearer);
            //echo "Auth Success";
            $response_string = "Authorization: Bearer " . $this->Cookie->read('bearer');
            $this->response->header(array(
                $response_string,
                "Auth: Success"
            ));

            $this->response->statusCode(200);
            echo $response_string;
            return $this->response;
        } else {
            echo "WRONG LOGIN OR PASSWORD";
            $this->response->header(array(
                //'Content-type: application/json',
                "Auth: Failure"
            ));
            $this->response->statusCode(401);
            return $this->response;
        }
    }

    /**
     * Get header Authorization
     * */
    private function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    private function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

}