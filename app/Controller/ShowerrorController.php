<?php

App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');
App::uses('DboSource', 'Model/DataSource');

class ShowerrorController extends AppController
{
	public $uses = array();

	public $components = array(
		'Session',
		'Error',
		);

	public $layout = "default";

	public function beforeFilter()
	{
		$auth_error = (isset($this->request->query['auth_error'])) ? $this->request->query['auth_error'] : null;
		$auth_error_text = (isset($this->request->query['auth_error_text'])) ? $this->request->query['auth_error_text'] : null;
		$this->set('auth_error', $auth_error);
		$this->set('auth_error_text', $auth_error_text);

		$show_login_form = true;
		$this->set('show_login_form', $show_login_form);
		$this->set('title', 1);

		parent::beforeFilter();
	}

	public function index()
	{
		$this->title_name = __("error_page_header");
		$this->set('title_page', 1);//Configure::read('SITENAME') . " - " . $this->title_name);
	}

	public function restricted()
	{
		$this->title_name = L("USER_RESTICTED_IP_ERROR");
		$this->set('title', Configure::read('SITENAME') . " - " . $this->title_name);
	}
}
