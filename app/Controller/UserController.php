<?php
/*
 * Контроллер действий пользователя в личном кабинете (настройки, профиль)
 * */
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class UserController extends AppController
{
	public $uses = array('User');

	public $layout = "main";

	public $components = array(
		'Session',
		'Breadcrumbs',
		'Uploader',
		'Error'
	);
	public $user_data;

	public $user_id;

	public function beforeFilter()
	{
	    /*
		$this->_checkMethod();
		$this->_checkAuth();
		$this->_user_data();
		$this->_last_activity();*/

		$this->Breadcrumbs->add(L('BO'), Router::url(array('plugin' => false, 'controller' => 'backoffice', 'action' => 'index')));

		//тикеты с ответами
        /*
		$unread_ticket_count = $this->Ticket->find('count',
			array(
				'conditions' => array(
					'user_id' => $this->user_data["User"]["id"],
					'answered' => 'admin',
					'readed' => 'NO',
				)
			)
		);
		$this->set('unread_ticket_count', $unread_ticket_count);
		//непрочтенные сообщения
		$unread_chat_count = $this->requestAction('/chat/new_messages/');
		$this->set('unread_chat_count', $unread_chat_count);
		*/



		parent::beforeFilter();
	}

    public function add_balance()
    {
        $this->set('title', "Пополнение баланса - " . L('SITENAME'));
    }

    public function game_log()
    {
        $this->set('title', "История ходов - " . L('SITENAME'));
    }

    public function wallet()
    {
        $this->set('title', "Ваш кошелек - " . L('SITENAME'));
    }

	private function _checkMethod()
	{
		if (!method_exists(__CLASS__, $this->request->params['action'])) {
			$this->redirect(array('controller' => 'showerror', 'action' => '/'));
			exit;
		}
	}

	private function _checkAuth()
	{
		$is_auth_user = ($this->Session->read('User')) ? true : false;
		if (!$is_auth_user) {
			$this->layout = 'blank';
			$this->set('page_title', __('back_office_not_auth_page_title'));

			if (isset($this->request->query['auth_error'])) {

				$this->set('auth_error', $this->request->query['auth_error']);
				$this->set('auth_error_text', $this->request->query['auth_error_text']);
			} else {
				$this->set('auth_error', '');
				$this->set('auth_error_text', '');
			}

			$this->render('auth_info');
			//если не индексная страница backoffice - производится редирект
			if ($this->request->params->controller !== "backoffice" OR $this->request->params->action != "index") {
				$this->redirect(array('controller' => 'index', 'action' => 'index'));
			}

		} else {
			$this->user_id = $this->Session->read('user_id');
			$this->set('title', L('BO'));
		}
	}

	private function _user_data()
	{
		//общие данные пользователя
		$user_id = $this->Session->read('user_id');
		$user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));
		$this->user_data = $user_data;
		$this->set('user_data', $user_data);
	}

	private function _last_activity()
	{
		//общие данные пользователя
		$user_id = $this->Session->read('user_id');
		$save_uptime = array('last_activity' => date("Y-m-d H:i:s"));
		$this->User->id = $user_id;
		$this->User->save($save_uptime);
	}

}