<?php

App::uses(
	'AppController', 'Controller'
);
App::uses('L10n', 'L10n');

/**
 * Компонент Капча
 */
class CaptchaComponent extends Component
{

	public $uses = array("");
	public $components = array('Session', 'Error');
	public $size = 4;//Количество символов, которые нужно набрать
	public $letters = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6', '7', '9');
	public $fon_let_amount = 30;//Количество символов, которые находятся на фоне
	public $width = 100;//Ширина изображения
	public $height = 60;//Высота изображения
	public $font;

	public function initialize(Controller $controller)
	{
		$this->font = Configure::read('WEBROOT') . DS . "cour.ttf";
	}

	public function image()
	{
		$code = $this->Session->read('captcha');
		$this->_captcha($code);

	}

	public function create()
	{
		$this->_create_captcha($this->size);
	}

	private function _create_captcha($size)
	{
		for ($i = 0; $i < $this->size; $i++) {
			$letter = $this->letters[rand(0, sizeof($this->letters) - 1)];
			$code[] = $letter;
			$this->Session->write('captcha', $code);
		}
	}

	private function _captcha($code)
	{

		$font_size = 17.5;//Размер шрифта
		$colors = array('10', '30', '50', '70', '90', '110', '130', '150', '170', '190', '210');
		$src = imagecreatetruecolor($this->width, $this->height);
		$fon = imagecolorallocate($src, 255, 255, 255);
		imagefill($src, 0, 0, $fon);

		for ($i = 0; $i < $this->fon_let_amount; $i++) {
			$color = imagecolorallocatealpha($src, rand(0, 255), rand(0, 255), rand(0, 255), 100);
			$font = $this->font;
			$letter = $this->letters[rand(0, sizeof($this->letters) - 1)];
			$size = rand($font_size - 2, $font_size + 2);
			imagettftext($src, $size, rand(0, 45), rand($this->width * 0.1, $this->width - $this->width * 0.1), rand($this->height * 0.2, $this->height), $color, $font, $letter);
		}

		for ($i = 0; $i < count($code); $i++) {
			$color = imagecolorallocatealpha($src, $colors[rand(0, sizeof($colors) - 1)], $colors[rand(0, sizeof($colors) - 1)], $colors[rand(0, sizeof($colors) - 1)], rand(20, 40));
			$font = $this->font;
			$letter = $code[$i];
			$size = rand($font_size * 2.1 - 2, $font_size * 2.1 + 2);
			$x = $i * $font_size + rand(7, 12);
			$y = (($this->height * 2) / 3) + rand(0, 5);
			imagettftext($src, $size, rand(0, 15), $x, $y, $color, $font, $letter);
		}

		header("Content-type: image/gif");
		imagegif($src);
		exit;
	}

}