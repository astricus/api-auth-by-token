<?php

App::uses(
	'AppController', 'Controller'
);
App::uses('L10n', 'L10n');

/**
 * Компонент Загрузчик файлов
 */
class UploaderComponent extends Component
{

	public $uses = array("Foto");
	public $components = array('Session', 'Error');
	public $temp_dir;

	function initialize(Controller $controller)
	{
		$this->controller = $controller;
	}

	public function beforeFilter()
	{
		$this->_checkUploadDirectoryExists();
		$this->temp_dir = Configure::read('FILE_TEMP_DIR');
	}

	private function _checkUploadDirectoryExists()
	{
		if (!file_exists(Configure::read('FILE_TEMP_DIR')) OR (!is_readable(Configure::read('FILE_TEMP_DIR')))) {
			$this->Error->setError('ERROR_101');
			return false;
		}
	}

	public function index()
	{
		$this->render('test_upload');
	}

	//инициатор загрузки
	public function upload($uploader_config, $file_array)
	{

		$result = $this->_upload_file($uploader_config, $file_array, "", "");
		return $result;
	}

	//переносчик файла
	public function transfer_file($file, $old_directory, $new_directory, $random_name = false)
	{
		$this->random_name = $random_name;
		$result = $this->_transfer_file($file, $old_directory, $new_directory, $this->random_name);
		return $result;
	}

	//загрузчик
	private function _upload_file($upload_type, $file, $new_filename = null, $directory = null)
	{

		$upload_types = array(
		    "USER_IMAGE_UPLOAD_CONFIG",
			"ADMIN_IMAGE_UPLOAD_CONFIG",
		);

		if (!in_array($upload_type, $upload_types)) {
			$this->Error->setError('ERROR_102');
		}

		$settings = Configure::read($upload_type);

		//проверка существования файла
		if (!file_exists($file['tmp_name'])) {
			$this->Error->setError('ERROR_103');
		}

		// проверки совместимости файла по выбранному конфигу
		$max_file_size = $settings['max_file_size'];
		$max_x = $settings['max_x'];
		$max_y = $settings['max_y'];

		$exts = $settings['ext'];

		$filename = $file['tmp_name'];
		$filesize = $file['size'];


		list($file_x, $file_y) = getimagesize($filename);

		if ($settings['image'] == 'true') {

			if ($file_x >= $max_x OR $file_y >= $max_y) {
				$this->Error->setError('ERROR_110');
			}

		}

		if (!in_array(ext($file['name']), $exts)) {
			$this->Error->setError('ERROR_112');
		}


		if ($filesize > $max_file_size) {
			$this->Error->setError('ERROR_111');
		}

		//проверка существования директории

		$dir = Configure::read('FILE_TEMP_DIR');
		if (!is_file($dir) && !is_dir($dir)) {
			mkdir($dir);
			chmod($dir, 0777);
		}


		$new_file = Configure::read('FILE_TEMP_DIR') . DS . $file['name'];
		if (!move_uploaded_file($file['tmp_name'], $new_file)) {
			$this->Error->setError('ERROR_113');
		} else {
			$data = array('file' => $file['name'], 'full_path' => $new_file);
			return $data;
		}

	}

	//Переносчик файла
	private function _transfer_file($file, $old_directory, $new_directory, $random_name = false)
	{

		//проверка существования файла
		if (!file_exists($old_directory . DS . $file)) {
			$this->Error->setError('ERROR_103');
		}

		//проверка существования новой директории и ее создание
		if (!file_exists($new_directory)) {
			if (!mkdir($new_directory, 0777)) {
				$this->Error->setError('ERROR_114');
			}
		}

		//перенос файла
		$old_file = $old_directory . DS . $file;

		//переименование файла
		if ($random_name) {
			$filename = substr(md5(time()), 0, 8);
			$ext = ext($file);
			$file = $filename . "." . $ext;
		}
		$new_file = $new_directory . DS . $file;
		if (!copy($old_file,
			$new_file)
		) {
			$this->Error->setError('ERROR_113');
			return false;
		} else {
			$this->new_filename = $file;
			unlink($old_file);
			return true;
		}

	}
}