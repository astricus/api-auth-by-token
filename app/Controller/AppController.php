<?php
//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');

//контроллер приложения

class AppController extends Controller
{
	public $uses = array();

	public $components = array(
		'Cookie',
		'Captcha',
		'Session',
		//'DebugKit.Toolbar'
	);

	public function beforeFilter()
	{
		$this->_setLanguage();
		$this->_checkSiteStatus();
		$this->_authUserGetData();
	}

	private function _checkSiteStatus()
	{
		$site_status = Configure::read('SITE_STATUS');
		if ($site_status == 'INWORK') {
			//статус сайта - в работе, доступен только для разработчиков
			$valid_dev_ips = Configure::read('VALID_DEV_IPS');
			if (!in_array($_SERVER['REMOTE_ADDR'], $valid_dev_ips)) {
				die("Сайт находится на реконструкции! Зайдите, пожалуйста, позднее!");
			}
		}
	}

	private function _authUserGetData()
	{
		$is_auth_user = ($this->Session->read('User')) ? true : false;
		if ($is_auth_user) {

			$user_id = $this->Session->read('user_id');

			//$this->Components->UserCom->last_activity();

			$user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));
			$this->user_data = $user_data;

			$this->set('user_login', $this->user_data['User']['login']);
            $this->set('user_score', $this->user_data['User']['score']);
            $this->set('user_coins', $this->user_data['User']['coins']);

			$img_path = site_url() .  "files" . "/" . "user_files" . "/" . "u" . $user_data["User"]["id"] . "/";
			$def_img_path = site_url() . "img/design/";
			$real_foto = Configure::read('USER_FILE_UPLOAD_DIR') . DS . "u" . $user_data["User"]["id"] . DS . $user_data['User']['main_foto'];

			if ((!empty($user_data['User']['main_foto'])) AND (file_exists($real_foto))) {
				$image = $img_path . $user_data['User']['main_foto'];
			} else {
				$image = $def_img_path . "default_user_icon.png";
			}
			$this->set('image', $image);
            $this->set('score_user_count', $this->user_data['User']['score']);
            $this->set('coin_user_count', $this->user_data['User']['coins']);

			$this->set('user_id', $this->user_data['User']['id']);
		} else {
            $this->set('score_user_count', 0);
            $this->set('coin_user_count', 0);
        }
	}

	private function _setLanguage()
	{
		$session_lang = ($this->Session->read('lang')) ? $this->Session->read('lang') : null;
		if ($session_lang == null) {
			$session_lang = Configure::read('DEF_LANG');

			Configure::load('rus_config');

		} else {
			if (!in_array($session_lang, Configure::read('VALID_LANGS'))) {
				$session_lang = Configure::read('DEF_LANG');
				Configure::load('rus_config');
			}
		}
		$this->Session->write('lang', $session_lang);
		$c = 0;
		foreach (Configure::read('VALID_LANGS') AS $lang) {

			if ($lang == $session_lang) {
				$key = $c;
			}
			$c++;
		}
		$locale = Configure::read('VALID_LANG_LOCALES')[$key];
		//запись в конфиг локали
		Configure::write('Config.language', $locale);
		Configure::load($locale . '_config');
		$this->Session->write('lang_locale', $locale);

		$lang_prefix = 'name_' . lang_prefix();
		$this->set('lang_prefix', $lang_prefix);
	}

	//проверка IP пользователя на авторизованные действия
	public function is_banned_ip()
	{
		$ip = get_ip();
		$check_restricted_ip = $this->Ip->find('all',
			array('conditions' =>
				array(
					'ip' => $ip
				)
			)
		);

		if (count($check_restricted_ip)) {
			return true;
		} else {
			return false;
		}
	}

	public function get_version()
	{
		echo phpinfo();
	}

	public function captcha()
	{
		$this->Captcha->image();
	}

}