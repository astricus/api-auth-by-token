<?php

//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');
//подключение файла с функциями
App::import('Lib', 'functions');

class AppError extends ErrorHandler
{
	function error404($params)
	{
		header("HTTP/1.0 404 Not Found");
		$this->controller->redirect(array('controller' => 'showerror', 'action' => '/'));
		parent::error404($params);
	}
}