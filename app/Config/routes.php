<?php
Router::connect('/index', array('controller' => 'index', 'action' => 'index'));
Router::connect('/', array('controller' => 'index', 'action' => 'index'));

// авторизация
Router::connect('/auth', array('controller' => 'index', 'action' => 'auth'));

Router::connect('/whoami', array('controller' => 'index', 'action' => 'whoami'));
Router::connect('/auth_form', array('controller' => 'index', 'action' => 'auth_form'));

CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';