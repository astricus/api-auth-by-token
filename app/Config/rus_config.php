<?php

$config = array(

	'MAIN_PAGE' => 'Начальная страница',

    'NEW_PAGE' => 'Страница новостей',

    'PASSWORD' => 'пароль',

    'ENTER' => 'Войти',

    'WRONG_LOGIN_OR_PASSWORD' => 'Неверный логин/пароль',

    'ENTER_SEC_CODE' => 'Введите код с картинки',

);