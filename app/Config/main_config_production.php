<?php

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('DEF_MAIL')) {
	define('DEF_MAIL', 'mail@startkit.local');
}

$config = array(

    'DEF_LANG' => 'RU',

    'DEF_LOCALE' => 'eng',

    'VALID_LANGS' => array('RU', 'EN'),

    'VALID_LANG_LOCALES' => array('rus', 'eng'),

	//общие

	'SITENAME' => 'moneygame.space',

	'SITE_URL' => 'http://moneygame.space/',

	'SITENAME' => 'Money Game - online instant lottery',

	'SITE_URL' => 'http://' . 'moneygame.space/',

	'SITEPHONE' => '',

	//записывается ли лог ошибок
	'ERROR_LOG_ACTIVE' => true,

	'IS_CLOUDFLARE' => "N",

	//почта сайта
	'SITE_MAIL' => DEF_MAIL,

	'CAN_SENT_EMAIL' => false,

	//Режим работы сайта
	'SITE_STATUS' => 'ACTIVE',

	//security
	'USER_AUTH_SALT' => md5('abrakadabra123'),

	'ADMIN_AUTH_SALT' => md5('abrakadabra123'),

	'SECURE_PAYMENT_SALT' => md5('karlson kotoriy jivet na krishe'),

	'MAIL_KEY_SALT' => md5('12312321321'),

	'ONLINE_USER_TIMER' => 60 * 15,

	//администратор
	//почта администратора
	'ADMIN_MAIL' => DEF_MAIL,

	'SMTP_CONFIG' => array(
		'port' => '465',
		'timeout' => '30',
		'auth' => true,
		'host' => 'ssl://smtp.gmail.com',
		'username' => '{GMAIL_USER}@gmail.com',
		'password' => '{GMAIL_PASS}',
	),

	'VALID_DEV_IPS' => array(
		'127.0.0.1',
	),

	//административная панель
	'ADMIN_PANEL' => 'Административная панель',
	'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'temp_image',

	//Директория загрузки изображений дизайна
	'DESIGN_IMAGE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'img' . DS . 'design',
	'DESIGN_IMAGE_DIR_RELATIVE' => '/img' . "/" . 'design' . "/",

	//Директория загрузки пользовательских изображений
	'USER_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'user_files',
	'USER_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'user_files',

	//Директория загрузки  изображений администратором и редактором
	'ADMIN_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'admin_files',

	//WEBROOT
	'WEBROOT' => dirname(__DIR__) . DS . 'webroot',

	//пользовательский конфиг загрузки изображений
	'USER_IMAGE_UPLOAD_CONFIG' => array(
		'ext' => array("jpg", "jpeg", "png", "gif"),
		'max_file_size' => 5 * 1000 * 1000,
		'max_x' => 2000,
		'max_y' => 2000,
		'image' => 'true',
	),

);