<?php
/**
 * системные функции
 */

/**
 * Обработка строк
 */

function get_mime_type($file)
{
	$mtype = false;
	if (function_exists('finfo_open')) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mtype = finfo_file($finfo, $file);
		finfo_close($finfo);
	} elseif (function_exists('mime_content_type')) {
		$mtype = mime_content_type($file);
	}
	return $mtype;
}

if (!function_exists('mb_ucfirst')) {
	function mb_ucfirst($str, $enc = 'utf-8')
	{
		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
	}
}

function translit($string)
{
	$replace = array(
		"'" => "",
		"`" => "",
		"а" => "a", "А" => "A",
		"б" => "b", "Б" => "B",
		"в" => "v", "В" => "V",
		"г" => "g", "Г" => "G",
		"д" => "d", "Д" => "D",
		"е" => "e", "Е" => "E",
		"ж" => "zh", "Ж" => "Zh",
		"з" => "z", "З" => "Z",
		"и" => "i", "И" => "I",
		"й" => "y", "Й" => "Y",
		"к" => "k", "К" => "K",
		"л" => "l", "Л" => "L",
		"м" => "m", "М" => "M",
		"н" => "n", "Н" => "N",
		"о" => "o", "О" => "O",
		"п" => "p", "П" => "P",
		"р" => "r", "Р" => "R",
		"с" => "s", "С" => "S",
		"т" => "t", "Т" => "T",
		"у" => "u", "У" => "U",
		"ф" => "f", "Ф" => "F",
		"х" => "h", "Х" => "H",
		"ц" => "c", "Ц" => "C",
		"ч" => "ch", "Ч" => "Ch",
		"ш" => "sh", "Ш" => "Sh",
		"щ" => "sch", "Щ" => "Sch",
		"ъ" => "", "Ъ" => "",
		"ы" => "y", "Ы" => "y",
		"ь" => "", "Ь" => "",
		"э" => "e", "Э" => "E",
		"ю" => "yu", "Ю" => "Yu",
		"я" => "ya", "Я" => "Ya",
		"і" => "i", "І" => "I",
		"ї" => "yi", "Ї" => "Yi",
		"є" => "e", "Є" => "E"
	);
	return $str = iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
}


/**
 * транспортные функции
 */

function response_ajax($array, $status = "success")
{
	$data = array('data' => $array, 'status' => $status);
	echo json_encode($data);
}


/**
 * функции-анализаторы
 */

function is_mobile()
{
	$user_agent = strtolower(getenv('HTTP_USER_AGENT'));
	$accept = strtolower(getenv('HTTP_ACCEPT'));

	if ((strpos($accept, 'text/vnd.wap.wml') !== false) ||
		(strpos($accept, 'application/vnd.wap.xhtml+xml') !== false)
	) {
		return 1; // Мобильный браузер обнаружен по HTTP-заголовкам
	}

	if (isset($_SERVER['HTTP_X_WAP_PROFILE']) ||
		isset($_SERVER['HTTP_PROFILE'])
	) {
		return 2; // Мобильный браузер обнаружен по установкам сервера
	}

	if (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|' .
		'wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|' .
		'lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|' .
		'mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|' .
		'm881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|' .
		'r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|' .
		'i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|' .
		'htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|' .
		'sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|' .
		'p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|' .
		'_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|' .
		's800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|' .
		'd736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |' .
		'sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|' .
		'up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|' .
		'pocket|kindle|mobile|psp|treo)/i', $user_agent)) {
		return 3; // Мобильный браузер обнаружен по сигнатуре User Agent
	}

	if (in_array(substr($user_agent, 0, 4),
		Array("1207", "3gso", "4thp", "501i", "502i", "503i", "504i", "505i", "506i",
			"6310", "6590", "770s", "802s", "a wa", "abac", "acer", "acoo", "acs-",
			"aiko", "airn", "alav", "alca", "alco", "amoi", "anex", "anny", "anyw",
			"aptu", "arch", "argo", "aste", "asus", "attw", "au-m", "audi", "aur ",
			"aus ", "avan", "beck", "bell", "benq", "bilb", "bird", "blac", "blaz",
			"brew", "brvw", "bumb", "bw-n", "bw-u", "c55/", "capi", "ccwa", "cdm-",
			"cell", "chtm", "cldc", "cmd-", "cond", "craw", "dait", "dall", "dang",
			"dbte", "dc-s", "devi", "dica", "dmob", "doco", "dopo", "ds-d", "ds12",
			"el49", "elai", "eml2", "emul", "eric", "erk0", "esl8", "ez40", "ez60",
			"ez70", "ezos", "ezwa", "ezze", "fake", "fetc", "fly-", "fly_", "g-mo",
			"g1 u", "g560", "gene", "gf-5", "go.w", "good", "grad", "grun", "haie",
			"hcit", "hd-m", "hd-p", "hd-t", "hei-", "hiba", "hipt", "hita", "hp i",
			"hpip", "hs-c", "htc ", "htc-", "htc_", "htca", "htcg", "htcp", "htcs",
			"htct", "http", "huaw", "hutc", "i-20", "i-go", "i-ma", "i230", "iac",
			"iac-", "iac/", "ibro", "idea", "ig01", "ikom", "im1k", "inno", "ipaq",
			"iris", "jata", "java", "jbro", "jemu", "jigs", "kddi", "keji", "kgt",
			"kgt/", "klon", "kpt ", "kwc-", "kyoc", "kyok", "leno", "lexi", "lg g",
			"lg-a", "lg-b", "lg-c", "lg-d", "lg-f", "lg-g", "lg-k", "lg-l", "lg-m",
			"lg-o", "lg-p", "lg-s", "lg-t", "lg-u", "lg-w", "lg/k", "lg/l", "lg/u",
			"lg50", "lg54", "lge-", "lge/", "libw", "lynx", "m-cr", "m1-w", "m3ga",
			"m50/", "mate", "maui", "maxo", "mc01", "mc21", "mcca", "medi", "merc",
			"meri", "midp", "mio8", "mioa", "mits", "mmef", "mo01", "mo02", "mobi",
			"mode", "modo", "mot ", "mot-", "moto", "motv", "mozz", "mt50", "mtp1",
			"mtv ", "mwbp", "mywa", "n100", "n101", "n102", "n202", "n203", "n300",
			"n302", "n500", "n502", "n505", "n700", "n701", "n710", "nec-", "nem-",
			"neon", "netf", "newg", "newt", "nok6", "noki", "nzph", "o2 x", "o2-x",
			"o2im", "opti", "opwv", "oran", "owg1", "p800", "palm", "pana", "pand",
			"pant", "pdxg", "pg-1", "pg-2", "pg-3", "pg-6", "pg-8", "pg-c", "pg13",
			"phil", "pire", "play", "pluc", "pn-2", "pock", "port", "pose", "prox",
			"psio", "pt-g", "qa-a", "qc-2", "qc-3", "qc-5", "qc-7", "qc07", "qc12",
			"qc21", "qc32", "qc60", "qci-", "qtek", "qwap", "r380", "r600", "raks",
			"rim9", "rove", "rozo", "s55/", "sage", "sama", "samm", "sams", "sany",
			"sava", "sc01", "sch-", "scoo", "scp-", "sdk/", "se47", "sec-", "sec0",
			"sec1", "semc", "send", "seri", "sgh-", "shar", "sie-", "siem", "sk-0",
			"sl45", "slid", "smal", "smar", "smb3", "smit", "smt5", "soft", "sony",
			"sp01", "sph-", "spv ", "spv-", "sy01", "symb", "t-mo", "t218", "t250",
			"t600", "t610", "t618", "tagt", "talk", "tcl-", "tdg-", "teli", "telm",
			"tim-", "topl", "tosh", "treo", "ts70", "tsm-", "tsm3", "tsm5", "tx-9",
			"up.b", "upg1", "upsi", "utst", "v400", "v750", "veri", "virg", "vite",
			"vk-v", "vk40", "vk50", "vk52", "vk53", "vm40", "voda", "vulc", "vx52",
			"vx53", "vx60", "vx61", "vx70", "vx80", "vx81", "vx83", "vx85", "vx98",
			"w3c ", "w3c-", "wap-", "wapa", "wapi", "wapj", "wapm", "wapp", "wapr",
			"waps", "wapt", "wapu", "wapv", "wapy", "webc", "whit", "wig ", "winc",
			"winw", "wmlb", "wonu", "x700", "xda-", "xda2", "xdag", "yas-", "your",
			"zeto", "zte-"))) {
		return 4; // Мобильный браузер обнаружен по сигнатуре User Agent
	}

	return 0; // Мобильный браузер не обнаружен
}

function get_os()
{

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$os_platform = "Unknown OS Platform";

	$os_array = array(
		'/windows nt 10/i' => 'Windows 10',
		'/windows nt 6.3/i' => 'Windows 8.1',
		'/windows nt 6.2/i' => 'Windows 8',
		'/windows nt 6.1/i' => 'Windows 7',
		'/windows nt 6.0/i' => 'Windows Vista',
		'/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
		'/windows nt 5.1/i' => 'Windows XP',
		'/windows xp/i' => 'Windows XP',
		'/windows nt 5.0/i' => 'Windows 2000',
		'/windows me/i' => 'Windows ME',
		'/win98/i' => 'Windows 98',
		'/win95/i' => 'Windows 95',
		'/win16/i' => 'Windows 3.11',
		'/macintosh|mac os x/i' => 'Mac OS X',
		'/mac_powerpc/i' => 'Mac OS 9',
		'/linux/i' => 'Linux',
		'/ubuntu/i' => 'Ubuntu',
		'/iphone/i' => 'iPhone',
		'/ipod/i' => 'iPod',
		'/ipad/i' => 'iPad',
		'/android/i' => 'Android',
		'/blackberry/i' => 'BlackBerry',
		'/webos/i' => 'Mobile'
	);

	foreach ($os_array as $regex => $value) {

		if (preg_match($regex, $user_agent)) {
			$os_platform = $value;
		}

	}

	return $os_platform;

}

function get_ua()
{

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$browser = "Unknown Browser";

	$browser_array = array(
		'/msie/i' => 'Internet Explorer',
		'/firefox/i' => 'Firefox',
		'/safari/i' => 'Safari',
		'/chrome/i' => 'Chrome',
		'/opera/i' => 'Opera',
		'/netscape/i' => 'Netscape',
		'/maxthon/i' => 'Maxthon',
		'/konqueror/i' => 'Konqueror',
		'/mobile/i' => 'Handheld Browser'
	);

	foreach ($browser_array as $regex => $value) {

		if (preg_match($regex, $user_agent)) {
			$browser = $value;
		}

	}

	return $browser;

}

function get_ip()
{
	if(Configure::read('IS_CLOUDFLARE') == 'Y'){
		return $_SERVER['HTTP_CF_IPCOUNTRY'];
	} else {
		return $_SERVER['REMOTE_ADDR'];
	}
}

function ext($filename)
{
	//$filename = strtolower($filename);
	return substr($filename, strrpos($filename, '.') + 1);
}

function get_hash($salt, $pass)
{
	return md5($salt . $pass . $salt);
}

function valid_hash($hash)
{
	if (strlen(trim($hash)) != 32) {
		return false;
	} else return true;
}

function site_url()
{
	return Configure::read('SITE_URL');
}

function set_title($add_sitename, $delimiter, $p1, $p2=null){
    $delimiter = " " . $delimiter . " ";
    $title = "";
    if($add_sitename) {
        $title .= L('SITENAME');

    }
    if(isset($p1)){
        $title .= $delimiter.$p1;
    }
    if(isset($p2)){
        $title .= $delimiter.$p2;
    }
    return  $title;
}

/* обработка времени*/
/**
 * дата из mysql_f to rus
 * 1 - день месяц год
 */

function MysqlDaySetTime($days){
	if($days == null) $day = 0;
	$day_text = "days";
	$start_date = date("Y-m-d H:i:s");
	$PREFIX = "+" . $days . " " . $day_text;
	return date('Y-m-d H:i:s', strtotime($PREFIX, strtotime($start_date)));
}

function only_date($date)
{
	return $date = substr($date, 0, 10);
}

function only_time($date)
{
	return $date = substr($date, 11, 5);
}

function lang_calendar($date, $format = 1, $time_view = true)
{
	if ($date == "0000-00-00 00:00:00") {
		$date = Configure::read('SITE_START_DATE');
	}

	$month = substr($date, 5, 2);
	$year = substr($date, 0, 4);
	$day = substr($date, 8, 2);
	$lang_month = "of_m_" . $month;
	$lang_month = L($lang_month);

	$h_prefix = L('h_prefix');
	$m_prefix = L('m_prefix');
	$y_prefix = L('y_prefix');

	if (strlen($date) == 19) {
		$hour = substr($date, 11, 2);
		$minute = substr($date, 14, 2);

		$time = $hour . $h_prefix . " " . $minute . " " . $m_prefix;

		$formatted_date = $day . " " . $lang_month . " " . $year . " " . $y_prefix . " ";
		if ($time_view) {
			$formatted_date = $formatted_date . $time;
		}
	} else {
		$formatted_date = $day . " " . $lang_month . " " . $year . " " . $y_prefix . " ";
	}
	return $formatted_date;

}

function dateJsToMysql($date)
{
	$day = substr($date, 0, 2);
	$year = substr($date, 6, 4);
	$month = substr($date, 3, 2);

	return $year . "-" . $month . "-" . $day;
}

function datepickerJsToMysql($date)
{
	$month = substr($date, 0, 2);
	$year = substr($date, 6, 4);
	$day = substr($date, 3, 2);

	return $year . "-" . $month . "-" . $day;
}

function datepickerMysqlToJs($date)
{
	$month = substr($date, 5, 2);
	$year = substr($date, 0, 4);
	$day = substr($date, 8, 2);

	return $month . "/" . $day . "/" . $year;
}

function day_separator($day_in_seconds, $days_only = false)
{


	$viewed_string = "";
	$days = floor($day_in_seconds / 86400);

	if ($days > 0) {
		$viewed_string .= $days . " " . word_count_format($days, L('DAYS_PLURAL'));
		$left = $day_in_seconds - $days * 86400;
	} else {
		$left = $day_in_seconds;
	}

	if ($days_only) {
		if (empty($viewed_string)) $viewed_string = L('1ST_DAY');
		return $viewed_string;
	}

	if($days>1){
		return $viewed_string;
	}

	$viewed_string .= " ";
	$hours = floor($left / 3600);
	if ($hours > 0) {

		$viewed_string .= $hours . " " . word_count_format($hours, L('HOURS_PLURAL'));
		$left = $left - $hours * 3600;
	}

	$viewed_string .= " ";
	$minutes = floor($left / 60);
	if ($minutes > 0) {
		$viewed_string .= $minutes . " " . word_count_format($minutes, L('MINUTES_PLURAL'));
		$left = $left - $minutes * 60;
	}

	$seconds = floor($left / 60);
	$viewed_string .= " ";
	if ($seconds > 0) {
		$viewed_string .= $seconds . " " . word_count_format($seconds, L('SECONDS_PLURAL'));
	}

	return $viewed_string;

}

function days_later($day_in_seconds, $real_time = '')
{
	$days = floor($day_in_seconds / 86400);
	if ($days == 0) {
		if($day_in_seconds<=60){
			return L('JUST_NOW');
		}
		else if($day_in_seconds>60 AND $day_in_seconds<=3600){
			$min = floor($day_in_seconds / 60);
			//return L('MINUTES_PLURAL');
			return $min . " " . word_count_format($min, L('MINUTES_PLURAL')) . " " . L('LATER');
		}

		//

		if($day_in_seconds>3600 AND $day_in_seconds<=86400){
			$hour = floor($day_in_seconds / 3600);
			return $hour . " " . word_count_format($hour, L('HOURS_PLURAL')) . " " . L('LATER');
		}
		//return L('TODAY');
	} elseif ($days == 1) {
		return L('YESTERDAY') . " " . L('AT') . " " . only_time($real_time);
	} elseif ($days > 1 AND $days < 30) {
		return $days . " " . word_count_format($days, L('DAYS_PLURAL')) . " " . L('LATER');
	} elseif ($days >= 30 AND $days < 365) {
		$month = floor($days / 30);
		return $month . " " . word_count_format($month, L('MONTH_PLURAL')) . " " . L('LATER');
	} elseif ($days >= 365) {
		$years = floor($days / 365);
		return $years . " " . word_count_format($years, L('YEAR_PLURAL')) . " " . L('LATER');
	}
}


function set_error($text, $error_header)
{
	echo "<div style='margin: auto; text-align: center;  background: #f8f8f8; border-radius: 10px; padding: 4px'>";
	echo "<h3 style='margin: auto; font-size: 24px; font-family: tahoma; '>" . $error_header . "</h3><br>";
	echo "<h3 style='margin: auto; font-size: 18px; font-family: tahoma; color: red'>" . $text . "</h3><br>";
	echo "<script>
		function goBack() {
			window.history.back();
		}
		</script>";
	echo "<a href='#' style='margin: auto; font-size: 26px; color: #888; font-family: tahoma' onclick=\"goBack()\"'>Вернуться к форме</a>";
	die();
}

function pgn_link($url, $page, $current = false, $ctrl)
{
	$data = array('page' => $page);
	$query_string = http_build_query($data);
	if ($current) {
		$class_selected = "selected_pgn_link";
	} else {
		$class_selected = "pgn_link";
	}

	if (!empty($url)) {
		$url = $url . "&";
	} else {
		$url = "";
	}

	return "<a class='" . $class_selected . "' href='" . site_url() . $ctrl . "?" . $url . $query_string . "'>" . $page . "</a>";
}

function paginator($page = 0, $pages = 0, $classes = "", $params = array(), $ctrl = "")
{
	if (is_array($params)) {
		$query_string = http_build_query($params);
	} else {
		$query_string = 0;
	}
	$PGN = "Страницы: ";
	if ($pages > 5) {
		$PGN .= pgn_link($query_string, $page - 1, $ctrl);
		$PGN .= pgn_link($query_string, $page, $ctrl);
		if ($page < $pages) {
			$PGN .= pgn_link($query_string, $page + 1, $ctrl);
		}
	} else {
		for ($c = 1; $c <= $pages; $c++) {
			if ($c == $page) {
				$current = true;
			} else {
				$current = false;
			}
			$PGN .= pgn_link($query_string, $c, $current, $ctrl);
		}
	}

	return $PGN;
}

function new_pgn_link($url, $page, $current = false, $ctrl, $dir_type = "")
{
	$page_text = "";
	echo $dir_type;
	$data = array('page' => $page);
	$query_string = http_build_query($data);
	if ($current) {
		$class_selected = "active";
		$page_text = $page;
	} else if ($dir_type == "prev") {
		$class_selected = "prev";
		$page_text = "&lt;";
	} else if ($dir_type == "next") {
		$class_selected = "next";
		$page_text = "&gt;";
	} else {
		$class_selected = "pgn_link";
		$page_text = $page;
	}

	if (!empty($url)) {
		$url = $url . "&";
	} else {
		$url = "";
	}

	return "<li  class='" . $class_selected . "'><a href='" . site_url() . $ctrl . "?" . $url . $query_string . "'>" . $page_text . "</a></li>";
}

function new_paginator($page = 0, $pages = 0, $classes = "", $params = array(), $ctrl = "")
{
	if (is_array($params)) {
		$query_string = http_build_query($params);
	} else {
		$query_string = 0;
	}
	$PGN = "Страницы: ";
	if ($pages > 5) {
		$PGN .= new_pgn_link($query_string, $page - 1, $ctrl, "");
		$PGN .= new_pgn_link($query_string, $page, $ctrl, "");
		if ($page < $pages) {
			$PGN .= new_pgn_link($query_string, $page + 1, $ctrl, "");
		}
	} else {
		for ($c = 1; $c <= $pages; $c++) {
			if ($c == $page) {
				$current = true;
			} else {
				$current = false;
			}
			$PGN .= new_pgn_link($query_string, $c, $current, $ctrl, "");
		}
	}

	if ($page > 1) {
		$PGN = new_pgn_link($query_string, $page - 1, false, $ctrl, "prev") . $PGN;
	}
	//die($pages);
	if ($page <= $pages - 1) {
		$PGN = $PGN . new_pgn_link($query_string, $page + 1, false, $ctrl, "next");
	}

	return $PGN;
}

function text_preview($str, $i)
{
	// Здесь приводим в кодировку win
	$str = iconv('UTF-8', 'windows-1251', $str);
	// Указываем также с какого по какой символ
	$str = substr($str, 0, $i);
	// Здесь возвращаем обратно в utf
	$str = iconv('windows-1251', 'UTF-8', $str);
	// Возвращаем $str
	return $str;
}

function lang_prefix()
{
	$def_lang = 'ru';
	$lang_session = (isset($_SESSION['lang'])) ? $_SESSION['lang'] : null;
	if ($lang_session == null) return $def_lang;
	$langs = array('ru', 'en', 'kz');
	$lang = strtolower($lang_session);
	if (in_array($lang, $langs)) {
		return $lang;
	} else return $def_lang;
}

function L($text)
{
	return Configure::read($text);
}

function C($config, $text)
{
	return $config[$text];
}

function is_valid_name($string, $min, $max)
{
	if ((!empty($string)) AND (mb_strlen($string) <= $max) AND (mb_strlen($string) >= $min)
		AND (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ ]+$/u', $string))
	) {

		return true;
	} else {
		return false;
	}
}


function generate_mail_key($id, $salt)
{

	return md5(md5(time()) . md5($id) . md5($salt));

}

function valid_password($pass)
{
	if (preg_match("/^[A-Z0-9_-]{6,32}$/i", $pass)) {
		return true;
	} else return false;
}

function valid_mail($email)
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return true;
	} else return false;
}

function js_str($s)
{
	return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
	$temp = array_map('js_str', $array);
	return '[' . implode(',', $temp) . ']';
}

function valid_minute($int)
{
	if ($int >= 0 AND $int <= 59) {
		return true;
	} else return false;
}

function valid_hour($int)
{
	if ($int >= 0 AND $int <= 23) {
		return true;
	} else return false;
}

function valid_date($date)
{
	return (bool)strtotime($date);
}

function get_wday($date)
{
	$day_of_week = date('w', strtotime($date));
	if ($day_of_week == 0) $day_of_week = 6;
	else $day_of_week = $day_of_week - 1;
	return $day_of_week;
}

function check_link($url, $domain)
{
	if (mb_strpos($url, $domain) != 0) {
		return false;
	} else return true;
}

function ts_from_date($date)
{

	return strtotime($date);
}

function get_year_from_mysql_date($date)
{
	$year = substr($date, 0, 4);
	if ((int)$year > 0 AND (int)$year < 9999) {
		return $year;
	} else return null;
}



function new_mail_hash($mail)
{
	return md5($mail . "MAIL" . time());
}

function bolder_text($string)
{
	if ((int)$string > 0) {
		return "<b>" . $string . "</b>";
	} else return $string;

}

//Ссылка на внешний ресурс
function outer_link($link)
{
	if (mb_strlen($link) > 0) {
		if (mb_strpos($link, "http") >= 0) {
			return "http://" . $link;
		} else {
			return $link;
		}
	} else {
		return "#";
	}
}

//функция текущей даты php
function now_date()
{
	return date("Y-m-d H:i:s");
}

//функция вывода заработной платы
function salary($value, $money_type, $payment_type, $delimiter, $not_defined_text)
{
	if (isset($value) AND is_numeric($value)) {
		return $value . " " . $money_type . " " . $delimiter . " " . $payment_type;
	} else {
		return $not_defined_text;
	}
}

function imageCreateFromAny($filepath)
{
	$type = exif_imagetype($filepath); // [] if you don't have exif you could use getImageSize()
	$allowedTypes = array(
		1,  // [] gif
		2,  // [] jpg
		3,  // [] png
		6   // [] bmp
	);
	if (!in_array($type, $allowedTypes)) {
		return false;
	}
	switch ($type) {
		case 1 :
			$im = imageCreateFromGif($filepath);
			break;
		case 2 :
			$im = imageCreateFromJpeg($filepath);
			break;
		case 3 :
			$im = imageCreateFromPng($filepath);
			break;
		case 6 :
			$im = imageCreateFromBmp($filepath);
			break;
	}
	return $im;
}

function imagesave($image_type, $image, $file)
{
	switch ($image_type) {
		case 1:
			//header('Content-type:image/gif');
			imagegif($image, $file);
			break;
		case 2:
			//header('Content-type:image/jpg');
			imagejpeg($image, $file, 100);
			//copy('test.jpg', $i);
			break; // best quality
		case 3:
			//header('Content-type:image/png');
			imagepng($image, $file, 0);
			break; // no compression
		default:
			echo '';
			break;
	}
}

// random float value
function random_float ($min,$max) {
	return ($min+lcg_value()*(abs($max-$min)));
}

function floatFontSize($string){
    if (strlen($string) >= 7){
        return 'fontXLarge';
    } else if(strlen($string) <= 6 AND (strlen($string) > 5)){
        return 'fontLarge';
    }
}

function microtime_float()
{
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}