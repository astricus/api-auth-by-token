window.vg = {
	faq: {
		init: function () {
			$("#p-faq .item .name").click(function (event) {
				event.preventDefault();
				$(this).next().toggle();
			});
		}
	},
	init: function () {
		$("header .city_link").click(function (event) {
			//event.preventDefault();
			$(this).find(".hover").toggle(0);
		});
		$(window).click(function (event) {
			if ($(event.target).closest(".hover").length > 0 || $(event.target).closest(".city_link").length > 0) {
				return;
			}
			$("header .city_link .hover").hide(0);
		});
		$("a.fancybox").fancybox();
		$("body").on('click', "button.fancybox", function (event) {
			event.preventDefault();
			$.fancybox.open($($(this).attr("data-href")));
		});

		$(".body_popup .close_popup").click(function (event) {
			//event.preventDefault();

			var n_id = $(this).attr('data-id');
			var transfer_data = null;
			var popup = $(this).parent();

			$.ajax({
				url: "/informer/deactivate/" + n_id,
				type: "get",
				dataType: 'json',
				data: transfer_data,
				beforeSend: function () {
					popup.hide(200);
				},
				success: function (data) {
				}
			});
		});

		$(document).on("click", ".get_up", function (event) {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			//console.log(121212);
			event.preventDefault();
			return false;
		});


	}
};

var score = 0;
var number = 0;

$(document).ready(function () {

    $( ".np_number" ).each(function(i){
        var initialDelay = i * 100 + 500;
        $( this ).delay(initialDelay).animate({
            backgroundColor: "#a100ff",
        }, 2000 ).animate({
            backgroundColor: "#4000ff",
        }, 2000 );
    });

	setInterval(function(){
        $( ".np_number" ).each(function(i){
            var initialDelay = i * 100 + 500;
            $( this ).delay(initialDelay).animate({
                backgroundColor: "#a100ff",
            }, 1400 ).animate({
                backgroundColor: "#4000ff",
            }, 1400 ).animate({
                backgroundColor: "#0080ff",
            }, 1200 );
		});

	}, 5000);
    $(document).on("click", ".np_number", function (e) {
        $(".np_number").removeClass("np_number_selected");
    	$(this).addClass("np_number_selected");
    	number = $(this).text()*1;
	});

    $(document).on("click", ".np_score", function (e) {
        $(".np_score").removeClass("np_score_selected");
        $(this).addClass("np_score_selected");
        score = $(this).text()*1;
    });

    var reset_game_field = function () {
        $(".winner_numbers").addClass("winner_numbers_empty");
        $(".score_win").addClass("score_empty");
        $(".rerun").addClass("rerun_empty");
        $(".winner_numbers_10x").text("x10");
        $(".winner_numbers_5x").text("x5");
        $(".winner_numbers_3x").text("x3");
        $(".winner_numbers_2x").text("x2");
        $(".win_block").html("");
    };

    var update_info = function(){
    	$.ajax({
            url: "/update_info/",
            type: "post",
            dataType: 'json',
            data: '',
            beforeSend: function () {
            },
            success: function (data) {
            	var info_prefix = "Знаете ли вы что... ";
				$(".some_info").text(info_prefix + " " + data.data.data);
            }
        });
    };

    update_info();
    setInterval(update_info, 5000);

    //last wins
    var last_wins = function(){
        $.ajax({
            url: "/last_wins/",
            type: "post",
            dataType: 'json',
            data: '',
            beforeSend: function () {
            },
            success: function (data) {
                $('.game_log').html('');
                for (var x = 0; x < data.data.data.length; x++) {
                    var new_gl_item = $('<div class="game_log_item">'+
                    '<a class="user_login inline" href="#">' + data.data.data[x].login + '</a>: c '+
                    '<img src="/img/design/stavki.png"> ' + data.data.data[x].score + ' выиграл <img src="/img/design/coins.png"> ' +
                        data.data.data[x].coins + '</div>');
                    $('.game_log').append(new_gl_item);
                }
            }
        });
    };

    last_wins();
    setInterval(last_wins, 5000);


    $(document).on("click", ".np_set", function () {
            var game_data = "number=" + number + "&score=" + score;
            $('#loading').show(0);

            $('#loading').fadeTo(1, 0.8);
            $.ajax({
                url: "/game/play",
                type: "post",
                dataType: 'json',
                data: game_data,
                beforeSend: function () {
                	reset_game_field();
                },
                success: function (data) {
                    data = data.data;
                    $('#loading').hide(0);
                    data = data.data;
                    if (data.error == '') {

                        console.log(data.current_score + " " + data.current_coins);
                        $('.np_score_result').text(data.current_score);
                        $('.np_coins_result').text(data.current_coins);

                        /*новое распределение выигрышей */
                        if (data.pos_2_g > 0){
                            $('.score_win_2').removeClass("score_empty");
                        }
                        if (data.pos_5_g > 0){
                            $('.score_win_5').removeClass("score_empty");
                        }
                        if (data.pos_10_g > 0){
                            $('.score_win_10').removeClass("score_empty");
						}
                        if (data.pos_20_g > 0){
                            $('.score_win_20').removeClass("score_empty");
                        }
                        if (data.pos_50_g > 0){
                            $('.score_win_50').removeClass("score_empty");
                        }

                        /*
                        * добавить ставки 2 и 5 в БД
                        * */


                        for (var x = 0; x < data.pos_2x.length; x++) {
                            console.log(data.pos_2x[x]);
                            if (data.pos_2x[x] != data.number) {
                                $('.winner_numbers_2x').eq(x).text(data.pos_2x[x]).addClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x no_win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                            else {
                                $('.winner_numbers_2x').eq(x).text(data.pos_2x[x]).removeClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                        }

                        for (var x = 0; x < data.pos_3x.length; x++) {
                            console.log(data.pos_3x[x]);
                            if (data.pos_3x[x] != data.number) {
                                $('.winner_numbers_3x').eq(x).text(data.pos_3x[x]).addClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x no_win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                            else {
                                $('.winner_numbers_3x').eq(x).text(data.pos_3x[x]).removeClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                        }

                        for (var x = 0; x < data.pos_5x.length; x++) {
                            console.log(data.pos_5x[x]);
                            if (data.pos_5x[x] != data.number) {
                                $('.winner_numbers_5x').eq(x).text(data.pos_5x[x]).addClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x no_win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                            else {
                                $('.winner_numbers_5x').eq(x).text(data.pos_5x[x]).removeClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                        }

                        for (var x = 0; x < data.pos_10x.length; x++) {
                            console.log(data.pos_10x[x]);
                            if (data.pos_10x[x] != data.number) {
                                $('.winner_numbers_10x').eq(x).text(data.pos_10x[x]).addClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x no_win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                            else {
                                $('.winner_numbers_10x').eq(x).text(data.pos_10x[x]).removeClass('winner_numbers_empty');
                                //$('.np_game_result').append('<div class="winner_numbers_2x win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                        }

                        var is_win = false;
                       if (data.win_sum > 0) {
                           priz = "<div class='win_coins_result'> +" + data.win_sum + '</div>';
                           $('.win_block').prepend(priz);
                           is_win = true;
                        }
                        if ((data.pos_10_g > 0) || (data.pos_20_g > 0) || (data.pos_50_g > 0) || (data.pos_2_g > 0) || (data.pos_5_g > 0)) {
                            var new_score = data.pos_10_g + data.pos_20_g + data.pos_50_g + data.pos_2_g + data.pos_5_g;
                            //console.log(data.pos_10_g + " / "+ data.pos_20_g  + " / " + data.pos_50_g + " / "+  data.pos_100_g);
                            priz = "<div class='win_score_result'>+" + new_score + '</div>';
                            $('.win_block').prepend(priz);
                            is_win = true;
                        }
                        if(!is_win){
                            priz = "<div class='no_prize'>Без выигрыша</div>";
                            $('.win_block').prepend(priz);
						}
                        return;




//выигрыш по ставкам
                        if ((data.pos_10_g > 0) || (data.pos_20_g > 0) || (data.pos_50_g > 0) || (data.pos_100_g > 0)) {
                            var new_score = data.pos_10_g + data.pos_20_g + data.pos_50_g + data.pos_100_g;
                            priz = "<img src='images/design/stavki.png' style='vertical-align: middle' width=25px> +" + new_gold_market;
                        }
//выигрыш суммы
                        else if (data.win_sum > 0) {
                            priz = "<div class='win_score_result'> +" + data.win_sum + '</div>';
                            $('.win_block').prepend(priz);
                        }

                        if (priz != "") {
                            $('#play_stat').prepend("<table style='margin-bottom:2px' cellpadding=3px><tr><td width=35px><div class=lucky_number>"
                                + data.number + "</div></td><td width=150px style='border: 1px dotted #ffe788;'><span class=yellow style='font-size: 10px'>"
                                + data.start_time + "</span></td><td width=90px style='border: 1px dotted #ffe788;'><p class=yellow><img src='images/design/stavki.png' style='vertical-align: middle' width=25px alt='ставка в игре' title='ставка в игре'>:"
                                + data.score + "</p></td><td width=200px style='border: 1px dotted #ffe788;'><p class=yellow>Приз: " + priz + "</p></td></tr></table>");
                        }
//представление выигрышных чисел
                        $('.np_game_result').html('');
                        $('.np_game_result').append('<div style="display: inline-table;"><p class=yellow style="font-size: 22px">2х:</p></div>');
                        for (var x = 0; x < data.pos_2x.length; x++) {
                            if (data.pos_2x[x] != data.number) {
                                $('.np_game_result').append('<div class="winner_numbers_2x no_win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class="winner_numbers_2x win_number_bg"><p class=yellow style="margin-top: 3px">' + data.pos_2x[x] + '</p></div>');
                            }
                        }

                        $('.np_game_result').append('<br><div style="display: inline-table;"><p class=yellow style="font-size: 22px">3х:</p></div>');
                        for (var x = 0; x < data.pos_3x.length; x++) {
                            if (data.pos_3x[x] != data.number) {
                                $('.np_game_result').append('<div class="winner_numbers_3x no_win_number_bg"><p class=yellow style="margin-top: 6px">' + data.pos_3x[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class="winner_numbers_3x win_number_bg"><p class=yellow style="margin-top: 6px">' + data.pos_3x[x] + '</p></div>');
                            }
                        }

                        $('.np_game_result').append('<br><div style="display: inline-table;"><p class=yellow style="font-size: 22px">5х:</p></div>');
                        for (var x = 0; x < data.pos_5x.length; x++) {
                            if (data.pos_5x[x] != data.number) {
                                $('.np_game_result').append('<div class="winner_numbers_5x no_win_number_bg"><p class=yellow style="margin-top: 12px">' + data.pos_5x[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class="winner_numbers_5x win_number_bg"><p class=yellow style="margin-top: 12px">' + data.pos_5x[x] + '</p></div>');
                            }
                        }

                        $('.np_game_result').append('<div style="display: inline-table; margin-left: 100px;"><p class=yellow style="font-size: 22px">10х:</p></div>');
                        for (var x = 0; x < data.pos_10x.length; x++) {
                            if (data.pos_10x[x] != data.number) {
                                $('.np_game_result').append('<div id=winner_numbers_10x style="background: #0c162a;""><p class=yellow_big style="margin-top: 25px">' + data.pos_10x[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div id=winner_numbers_10x style="background: #ff0000;"><p class=yellow_big style="margin-top: 25px">' + data.pos_10x[x] + '</p></div>');
                            }
                        }

//ставки
                        $('.np_game_result').append("<br>");
                        $('.np_game_result').append('<div style="display: inline-table;"><p class=yellow style="font-size: 22px"><img src="images/design/stavki.png" width=30px>+10</p></div>');
                        for (var x = 0; x < data.stavki_10.length; x++) {
                            if (data.stavki_10[x] != data.number) {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #0c162a;""><p class=yellow style="margin-top: 3px">' + data.stavki_10[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #ff0000;"><p class=yellow style="margin-top: 3px">' + data.stavki_10[x] + '</p></div>');
                            }
                        }
                        $('.np_game_result').append('<div style="display: inline-table; margin-left: 100px;"><p class=yellow style="font-size: 22px"><img src="images/design/stavki.png" width=30px>+20</p></div>');
                        for (var x = 0; x < data.stavki_20.length; x++) {
                            if (data.stavki_20[x] != data.number) {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #0c162a;""><p class=yellow style="margin-top: 3px">' + data.stavki_20[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #ff0000;"><p class=yellow style="margin-top: 3px">' + data.stavki_20[x] + '</p></div>');
                            }
                        }
                        $('.np_game_result').append('<br><div style="display: inline-table;"><p class=yellow style="font-size: 22px"><img src="images/design/stavki.png" width=30px>+50</p></div>');
                        for (var x = 0; x < data.stavki_50.length; x++) {
                            if (data.stavki_50[x] != data.number) {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #0c162a;""><p class=yellow style="margin-top: 3px">' + data.stavki_50[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #ff0000;"><p class=yellow style="margin-top: 3px">' + data.stavki_50[x] + '</p></div>');
                            }
                        }
                        $('.np_game_result').append('<div style="display: inline-table; margin-left: 100px;"><p class=yellow style="font-size: 22px"><img src="images/design/stavki.png" width=30px>+100</p></div>');
                        for (var x = 0; x < data.stavki_100.length; x++) {
                            if (data.stavki_100[x] != data.number) {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #0c162a;""><p class=yellow style="margin-top: 3px">' + data.stavki_100[x] + '</p></div>');
                            }
                            else {
                                $('.np_game_result').append('<div class=winner_numbers_2x style="background: #ff0000;"><p class=yellow style="margin-top: 3px">' + data.stavki_100[x] + '</p></div>');
                            }
                        }

                        //победа!!!

                        if (data.win_sum > 0) {
                            $('#play_ground').fadeTo(500, 0);
                            $('#victory').fadeTo(500, 1);
                            $('#victory').html("<p class=yellow_big>ПОБЕДА!!!</p><img src='images/design/victory.gif'>");
							/*
							 var audioElement = document.createElement('audio');
							 audioElement.setAttribute('src', 'salut.mp3');
							 audioElement.setAttribute('autoplay', 'autoplay');
							 //audioElement.load()
							 $.get();
							 audioElement.addEventListener("load", function() {
							 audioElement.play();
							 }, true);
							 audioElement.play();
							 */
                            var show_victory = setTimeout(function () {
                                $('#play_ground').fadeTo(500, 1);
                                $('#victory').fadeTo(500, 0);
                                $('#victory').hide(500);
                            }, 4000);
                        }

                    }
                    else if (data.error == 'wrong_score') {
                        $('#error_message').html("Вы указали некорректную ставку! Ставка может быть от 1 до 10 000 рублей!");
                        $('#error_message').show(400);
                    }
                    else if (data.error == 'empty_gold_market') {
                        $('#error_message').html("Вы не можете играть, если у вас нет ставок!");
                        $('#error_message').show(400);
                    }
                    else if (data.error == 'score_must_be_less_then_gold_market') {
                        $('#error_message').html("Вы указали ставку, которой не располагаете в наличии!");
                        $('#error_message').show(400);
                    }
                    else if (data.error == 'wrong_number') {
                        $('#error_message').html("Вы указали некорректное число! Число может быть от 0 до 99!");
                        $('#error_message').show(400);
                    }
                    else if (data.error == 'empty_user') {
                        $('#error_message').html("Отсутствует номер игрока! Пожалуйста, авторизуйтесь или зарегистрируйтесь на сайте!");
                        $('#error_message').show(400);
                    }
//прозрачность розыгранных чисел
                    $('.lucky_number').each(function () {
                        var op = $(this).attr('div_opacity');
                        $(this).fadeTo(0, op);
                    });
                },
                complete: function () {

                }
            });
        return false;
    });

});