<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Вход в Мою Карту
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="w1-verification" content="170404989232"/>
	<?php
	echo $this->Html->meta('icon');
	//echo $this->Html->css('old_main');
	?>


	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,500,700,400italic&subset=latin,cyrillic'
	      rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.0.6/css/swiper.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
	<?
	echo $this->Html->css('style');
	//echo $this->Html->script('jquery');
	echo $this->Html->script('jquery-1.11.2.min'); ?>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<?
	echo $this->Html->script('old_main');
	echo $this->Html->css('new_main');

	?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.0.6/js/swiper.min.js"></script>

	<?

	echo $this->Html->script('perfect-scrollbar');
	echo $this->Html->script('main');


	echo $this->Html->script('search_ajax');
	echo $this->Html->script('city_header');
	echo $this->Html->script('gallery_profile');
	echo $this->Html->script('filter_country_city');
	echo $this->Html->script('backoffice/hint'); ?>

	<script>
		$(function () {
			vg.init();
			vg.main.init();
		});
	</script>

	<?
	//echo $this->Html->script('datepicker');

	//echo $this->Html->script('cities');
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>

	<? echo $this->element('scripts'); ?>

</head>
<body>

<div id="wrapper" class="white_bg">
	<header>
		<div class="fwidth">
			<a href="<?php echo site_url() ?>" class="logo">
				<img src="<?php echo site_url() ?>img/d/logo_1.png" alt="" width="82px">
			</a>
		</div>
		<div class="clr"></div>
	</header>

	<div class="main_nav_wrapper">
		<div class="main_nav">
			<a class="main_nav_element" href="invite/search/">Приглашения</a>
			<a class="main_nav_element" href="event/search/">События</a>
			<a class="main_nav_element" href="place/search/">Места</a>
			<a class="main_nav_element" href="user/search/">Люди</a>
			<a class="main_nav_element" href="feddback/">Отзывы</a>
			<a class="main_nav_element" href="locator/">Локатор</a>
			<a class="main_nav_element" href="about/">О проекте</a>
			<a class="main_nav_element" href="news/">Новости</a>
			<a class="main_nav_element" href="adv/">Реклама</a>
			<a class="main_nav_element" href="promo/">Организаторам</a>
			<a class="main_nav_element" href="faq/">Обратная связь</a>
		</div>
	</div>

	<?php echo $this->Session->flash(); ?>

	<?php echo $this->fetch('content'); ?>

</div>

<? echo $this->element('footer_main'); ?>

</body>
</html>