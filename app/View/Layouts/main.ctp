<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<title>
		<?php echo $title; ?>
	</title>
	<? if (isset($keywords)) { ?>
		<meta name="keywords" content="<? echo $keywords ?>"/>
	<? } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
	echo $this->Html->meta('icon');
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/9e3d075a1d.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <?
	echo $this->Html->css('jquery-ui');
	echo $this->Html->css('style');
    //echo $this->Html->css('style_vii');
    echo $this->Html->css('font-awesome.min');
	echo $this->Html->script('jquery-ui.min');
	echo $this->Html->script('main');
    //echo $this->Html->script('style-edit');
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	echo $this->element('scripts'); ?>
</head>
<body id="">

<header>
	<div class="fwidth">
		<a href="<?php echo site_url() ?>" class="logo">
            MoneyGame
		</a>
	</div>
</header>
<div class="content" >

	<div class="main_content">
		<?
        if($this->Session->check("user_id")){
            echo $this->element('topmenu_user');
        } else {
            echo $this->element('topmenu');
        }
		?>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>

	</div>

</div>
<?/*
<script src="https://cdnjs.cloudflare.com/ajax/libs/sketch.js/1.0/sketch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dat-gui/0.6.2/dat.gui.min.js"></script>
<script src="main.js"></script>*/?>
<?php //echo $this->Html->script('bg'); ?>
<?
echo $this->element('footer_main');
?>
</body>
</html>