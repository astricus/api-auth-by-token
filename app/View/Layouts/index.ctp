<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<title>
		<?php echo $title; ?>
	</title>
	<? if (isset($keywords)) { ?>
		<meta name="keywords" content="<? echo $keywords ?>"/>
	<? } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
	echo $this->Html->meta('icon');
	?>

	<!--[if IE]>
	<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300,500,700,400italic&subset=latin,cyrillic'
	      rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.0.6/css/swiper.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<?
	echo $this->Html->css('css/font-awesome.min');
	echo $this->Html->css('jquery-ui');
	echo $this->Html->css('style');
	echo $this->Html->css('new_main');
	//echo $this->Html->script('jquery');
	//echo $this->Html->script('jquery-1.11.2.min');
	echo $this->Html->script('jquery-ui.min');
	echo $this->Html->script('easing');
	//echo $this->Html->script('old_main');
	?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.0.6/js/swiper.min.js"></script>
	<script src="https://use.fontawesome.com/9e3d075a1d.js"></script>

	<?

	echo $this->Html->script('main');
	//echo $this->Html->script('hint');
	echo $this->Html->script('nav');
	echo $this->Html->script('search_ajax');
	echo $this->Html->script('city_header');
	echo $this->Html->script('gallery_profile');
	echo $this->Html->script('jquery.md5');
	echo $this->Html->script('like');
	echo $this->Html->script('panels');
	echo $this->Html->script('ajax');
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	echo $this->element('scripts'); ?>
	<script>
		$(function () {
			vg.init();
			//vg.main.init();
		});
	</script>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Russo+One" rel="stylesheet">
</head>
<body>

<?php if ($this->Session->check('User')) {
	$user_type = $this->Session->read('user_type');
	$link_BO_text = 'GO_TO_BO';
	$link_BO = 'backoffice';
} ?>

<? //echo $this->element('getup'); ?>

<div id="login" class="popup">
	<form action="<?php echo site_url(); ?>/login/login" method="post" name="login_form" class="g-form">

		<?php if (isset($backoffice)) { ?>
			<input type="hidden" name="data[User][backoffice]" value="1">
		<? } ?>

		<div class="title"><?php echo L('ENTER_IN_BO'); ?></div>
		<div class="line">
			<span class="w50">
				<input type="text" placeholder="<?php echo L('YOUR_EMAIL') ?>" name="data[User][mail]">
			</span>
			<span class="w50">
				<input type="password" placeholder="<?php echo L('PASSWORD') ?>" name="data[User][password]">
			</span>
		</div>

		<div class="line">
			<div class="w50">
				<button class="btn red"><?php echo L('ENTER'); ?></button>
				<a href="#forgot-pass" class="pull-left fancybox"><?php echo L('FORGOT_PASSWORD'); ?></a>
				<a href="<? echo site_url() ?>/register/" class="pull-right"><?php echo L('REGISTRATION'); ?></a>
			</div>
		</div>

		<?php if ($auth_error) { ?>
			<div class="auth_error">
				<div class="error_icon">!</div> <?php echo $auth_error_text; ?></div>
		<? } ?>

	</form>
</div>

<div id="forgot-pass" class="popup">
	<form action="#" class="g-form">
		<div class="title"><?php echo L('BO_RECOVERY_TITLE') ?></div>
		<label class="line">
        <span class="w75">
          <input type="text" placeholder="<?php echo L('RECOVERY_SET_USER_DATA') ?>">
        </span>
		</label>

		<div class="line">
			<div class="w50">
				<button data-href="#forgot-pass-result"
				        class="btn red fancybox"><?php echo L('RECOVERY_INIT_BUTTON_TITLE') ?></button>
			</div>
		</div>
	</form>
</div>

<div id="forgot-pass-result" class="popup">
	<div class="title"><?php echo L('BO_RECOVERY_TITLE') ?></div>
	<div class="result"><?php echo L('RECOVERY_EMAIL_SENDED_SUCCESS') ?></div>
</div>


<header>
	<div class="fwidth">
		<a href="<?php echo site_url() ?>" class="logo">
			Моя Карта
			<!-- <img src="<?php echo site_url() ?>img/d/logo_1.png" alt="" width="82px"> -->
			<span class="logo_prefix">Геосоциальная сеть</span>
		</a>

		<?php if (!$this->Session->check('User')) { ?>
			<a href="#login" class="pull-right btn-blue fancybox"><span><?php echo L('ENTER'); ?></span></a>
		<? } else { ?>

			<ul class="logged pull-right">
				<li><a href="<?php echo site_url() . $link_BO . "/" ?>" class="usr"
				       style="background-image: url(<? echo $image ?>);"></a></li>
				<li><a href="<?php echo site_url() . $link_BO . "?mode=messages/" ?>" class="msg"><sup
							class="bage"><? echo $unread_mess_count ?></sup></a></li>
				<li><a href="<?php echo site_url() . "login/logout/"; ?>" title='<?php echo L('QUIT') ?>'
				       class="logout fa fa-sign-out" aria-hidden="true"></a></li>
			</ul>
			<div class="h-clr"></div>

		<? } ?>

		<div class="h-clr"></div>

		<div class="header_search">

			<p class="icon-search search_icon"></p>
			<form class="search_block" action="<? echo site_url() ?>search/" method="get">
				<input name="q" type=text class="search_link" placeholder="<?php echo L('SEARCH') ?>">
			</form>
		</div>

	</div>
	<div class="clr"></div>
</header>

<div class="content">

	<div class="main_content_bg"></div>

	<div class="main_content_bg_color"></div>

	<div class="main_content">

		<script>
			function getRandomInt(min, max) {
				return Math.floor(Math.random() * (max - min + 1) + min);
			}

			function removeA(arr) {
				var what, a = arguments, L = a.length, ax;
				while (L > 1 && arr.length) {
					what = a[--L];
					while ((ax= arr.indexOf(what)) !== -1) {
						arr.splice(ax, 1);
					}
				}
				return arr;
			}
//			var ary = ['three', 'seven', 'eleven'];
//			removeA(ary, 'seven');

			var MC_height = $(window).height()+128;

			$(".main_content_bg").css({'height': MC_height});
			$(".main_content_bg_color").css({'height': MC_height});
			$(".main_content").css({'height': MC_height});

			var Xcounter = $(document).width()*1;
			var Lcounter =  $(".main_content_bg").height()*1;
			var PS = Math.floor(Xcounter*Lcounter/(72*72)) + Math.floor(Xcounter/72);
			console.log(PS);

			$.ajax({
				url: 'https://randomuser.me/api/?results=100',
				dataType: 'json',
				success: function (data) {
					console.log(data);
					for (var i = 0; i < PS; i++) {
						x = i % 30;
						var new_user_ava = $("<div class='pattern_ava'><img class='pattern_ava_content' src='" + data.results[x].picture.medium + "'></div>");
						opacity = 1;
						$(".main_content_bg").append(new_user_ava);
					}

				}
			});

			speedX = 0.3;
			docWidth = $(document).width();
			var bg_height = $(".main_content_bg").height() * 1;
			var defAva = '/img/default-avatar.png';

			var animatedDUmp = [];
			var animateImages = function () {
				var randomFrom = getRandomInt(1, 500);
				var randomTo = getRandomInt(1, 500);
				while (animatedDUmp.indexOf(randomFrom, animatedDUmp)>=0) {
					randomFrom = getRandomInt(1, 500);
				}

				while (randomFrom == randomTo && indexOf(randomTo, animatedDUmp)>=0) {
					randomTo = getRandomInt(1, 500);
				}
				animatedDUmp.push(randomTo);
				animatedDUmp.push(randomFrom);

				fromObj = $(".pattern_ava .pattern_ava_content").eq(randomFrom);
				toObj = $(".pattern_ava .pattern_ava_content").eq(randomTo);

				imgFrom = fromObj.attr('src');
				imgTo = toObj.attr('src');
				var smooth = false;
				if(smooth == true) {

					fromObj.fadeOut(1500, function () {
						fromObj.delay(300).attr('src', imgTo);
						fromObj.fadeIn(1500, function () {

							toObj.delay(1200).fadeOut(1500, function () {
								toObj.delay(300).attr('src', imgFrom);
								toObj.fadeIn(1500, function () {
									removeA(animatedDUmp, randomTo)
									removeA(animatedDUmp, randomFrom);
								});

							});

						});
					});
				} else {
					fromObj.delay(300).attr('src', imgTo);
					toObj.delay(300).attr('src',imgFrom);
				}
				while(animatedDUmp.length>30){
					animatedDUmp.shift();
				}

			};

			setInterval(function () {
				//animateImages();
			}, 250);


			$(document).scroll(function () {
				var cur_scroll = $(document).scrollTop();
				var doc_height = $(document).height() * 1;
				var window_height = $(window).height() * 1;
				var layout_height = $(".layout").height() * 1;
				if(cur_scroll<800) {
					var opacity = cur_scroll/900;
					if(opacity>0.8)  opacity = 0.8;
					if(opacity<0.1)  opacity = 0.1;
					var BgParam = 0;//255 - cur_scroll*255/800;
					var bgcolor = 'rgba(' + BgParam +', ' + BgParam +', ' + BgParam +', ' + opacity + ')';
					$(".main_content_bg").css({'top': cur_scroll / 2});
					//$(".main_content_bg").
					$(".main_content_bg_color").css({'top': cur_scroll / 2,
						'backgroundColor': bgcolor});
				}

			});

		</script>

		<div class="fwidth main_screen">

			<?
			//echo $this->element('topmenu');
			?>
			<?
			echo $this->element('news_popup');
			?>

			<div class="promo_top">

				<div class="welcome_text">

					<div class="logo_holder">
						<div class="fa fa-heart logo_heart_icon">
							<div class="fa fa-map-marker logo_marker_icon"></div>
						</div>

						<div class="main_color_2 main_name inline" style="">Моя Карта</div>
					</div>

				</div>

				<div class="welcome_image">
					<h4 class="welcome_label" style="font-family: 'Roboto', sans-serif;"><b>Моя Карта</b> - Cоциальная сеть с географией, сервис для объединения людей при
						помощи
						карт. Создавайте свои собственные карты-сообщества, приглашайте друзей и знакомых. Находите полезные
						знакомства,
						организуйте совместно досуг, устраивайте мероприятия и приглашайте желающих, создавайте
						исследовательские и профессиональные карты, планируйте маршруты поездок и путешествий, ищите попутчиков,
						показывайте свои обьявления на карте, а мы поможем вам в этом.
					</h4>

				</div>

				<div class="welcome_device">
					<img src="/img/devices.png"/>
				</div>
			</div>

			<hr>

			<div class="info_elements">

				<a class="gray_text info_element inline_element inline" href="#">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABZElEQVRoge2YsWrDMBRF8yM1mgqBQrJEs7EJ+b1+RBcv9YOSpaXKZLLZdIk9COQ/SMCBjK9LTFMnbSTFRBa8C3ex4d53rKfFoxGJdH+lq3UIIlMgMpWu1qHrHPPiz6wGkeHRynWOsU5KEUSGrnMu6hBOWRPzdB9xtY85nrpb3H2v66s5EVdNzNNDOGXmw0ezrXVxXwBHN9FsawTRxBz+Cvt6ejwr3kzGxsOb5jQxB32Af77+R/BwVrxigTGAcU7E9S/51eK395/iBFDYAhjm9AKwmYxxOV8gJICQAC7nC6sVssnpBaAtFyxAwQKr4W1zegNwZQJwbW2AopI4RBOAaxNA6+eX11/u+zkBEMDQAby/xN4DeL9CBEAANwJ4f4m9B/B+hQiAAG4E8P4SDx4gr+TO9bBd56WsTU4AXA98wfq/14tSsSGdQl7JXVEqpg3QQhSVhLyUtcu1KSoJxsOTSCTS3fQNbYz0JsKsCzEAAAAASUVORK5CYII="
						class="info_element_icon">

					<div class="data_info_counter"><?= $stat_data['day_online'] ?></div>
					<div class="data_info">дней онлайн</div>
				</a>

				<a class="gray_text info_element inline_element inline" href="/popular/">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAD70lEQVRoge3Xz0+adxwHcP8JD7t5WLJDD91ptyVNdtlhtx2WbLc9tdqhj0qZqwYtPK7UEr8aRJnMbXbIUKPI4MFS7PABdXPNNm1NfzhFC/JozAMEzRrTQMh7Jx4hlMlDwUr2fJLv4XkSPnxefH99qKmRQw455KjagDD3LuJsD2LsFnb1x+CN98GPfFCJ/OXKWYMoewlRpwYxlkOMhThCzMnY7UuAN3LgTa3lyl+ZogsBJGKKyS+96OzlUajoYgDZI0L+QcSwgMjAZ1LzSwcUU7RUQPaQmF8G/K8B8dWunOfjZxq85GfPDyAlsOi33sGn+lnofrTgZfBrsViXsRmEpuAepoEQg4MHahiUl2FQXsFRcOp8AG5Y5lFb7xfH52RGBIyqG0BoCqPqBiDE4PFcOwhNgdAUtldGgRiL7ZVRuM3d4FfvnA0gsTmJtOASn9+7vpADeKuBEwHhxU64h2lEljuBEIPklhYBSxsWp3rFHKbrV0FoCtbea5UHBCZugdAUbHqV+O7DHl8O4ELbvKRN7PqmC4SmEJi4VXnAFPkKhKYwqGpAct8BxFisPbqLd1ruo7bejzqFDz6fUfIpdLxrB2IsnnImOIfVecupbID4hg3cTzo8f/BdzvvDDR3WVgZwuKEr+RhNCy4MtTeC0BQm+9rPzzEq5R7ILKffHX3VCUCMfeUdUTWA1MHPEB5bqxdg06tAaAq+8ZvVCTCrm0BoCnZDx9kBnnImmDuvIGBpA0IMUkEtnvs7kApqJQMO1sexPK3PazMqCshM+0ArheSWFp6RFhCaAjvUfD670bTgwr3vtbAbOnAUnMLWr9/C0nMVv00ogRCDydtfgNAUxjSNRQMOeTccCx64/B6khAoDwn/8IDZky9P6vD0QX+3C4rgS0T+7Ci+nrHzxsBsXVZzYinx005eHKCsgue+AtfcazOomHKyP/+cmzrTTLmNzQcCg3ZvTS9XW+zH9y73yAlICi7uLHjgWPIiH3UWdQukdLWaIAoSmMEMUSO9o8eLJDdj7m2A3dIi9VP90PsDmLSMgJbD4+PZJx3lByWFv230qACEGiYfd+GtWhcTDbiDE4JHrS3H5bS6ZgRiLnb/nUKc4WUIXVRwOeXf5AJ4lT94vpLPNl3QPHK13Y0zTiDGmRexAMwiD3Ythh/eVM/xaANafD2CspQHeyDH6Yt+N99Un/7rqFBw2n81VDyCDMDu9MNi9+cVXA+DUIQPeBCDOrkFg1RCcbyMydBl7xgAi5LhsgKz85QRwiDo1iLKXCn6WN7WCN3LY7UuUADg1fymAkpMWxFSi6Jwvfp3pK5Rzb+QT7A0uIdybrER+OeSQQ46zj38BMlWvGzC7Tf0AAAAASUVORK5CYII="
						class="info_element_icon">

					<div
						class="data_info_counter <?= floatFontSize($stat_data['active_maps_count']) ?>"><?= $stat_data['active_maps_count'] ?></div>
					<div class="data_info">публичных карт</div>
				</a>

				<a class="gray_text info_element inline_element inline" href="#">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACi0lEQVRoge2ZIU8cQRiGH8EPQCIQiAqSUiSizLeZFEgQiDMlAUkq60laHGkRSCwJoqLB0JAKSBAIRJNCimgCpo60iuTE4RBvxS20Dbdze3uzu6S5Nxlz2f3meXZnZ2b3YJBBBokWecaVsCbjSI5LGS0ZTSV8l3GghDV5xuvmfBAZDSWcy1Cu1j62UTc38owp4TQ3+EORU3me1APveCnja2H4P+2bEparhnfp+O4X/q7dyjFbFbyPdOU73YkX5Qv0M+ZzPBPlwhuNHCAtOTZkNOQZlWdURiP9Lc+wK292yjFVHsszlnm+Z0zGcZe7cF4WfBLs2LErz3DXOp5hOXa7SCTxBRxvA53+kDGTu5Yxk56TVe9NfAHjKHD1NwrU2wwIHJQhcBYQWOq5nmMpIHBWhsCvzA49Ez3X80wEBH7GF2jvLjt3OM3Tnus9Dwg4LuMLJOwFZo3FAvUWA/X24guEHroiD3F7YcsaQpvxBRzzgQ5boQXsQa32gpa9Kjvm4wt4hmRcBSSOe6gTWo2v5BmKLgAgx3qgY6UrbOaClu5kP3apsV4KPNzf+uzO74ZTP5u5HoZiMQnjcw6Jou1TqfAAcizIuCkB/kaOhdIFUont6AKO7UrgATTNnIzriALXmmauMgEAGTsRBXYqhU8FpmRcRIC/kDFVuQCAHKsRxv5qLfAA8ozIOOxD4FCekdoEAOSY7ePqV/Mxq1tkbBUQ2Kqb+z4yJpXwJTd8+9jJurn/iRwrMm5zCNzKsVI3b8fI+PAo5/y8kTEp4yQAf6KEZ3VzBiPHsoxmB/hmkc8vtUTG+w4C7+rmyp30JWb/L/h9eUbr5uop8oyr/QbWepT/TOaJjNcyXtXNMcgg/3N+A/7Cej0R244OAAAAAElFTkSuQmCC"
						class="info_element_icon">

					<div class="data_info_counter <?= floatFontSize($stat_data['active_map_items_count']) ?>"><?
						echo $stat_data['active_map_items_count'];
						?></div>
					<div class="data_info">активных меток</div>
				</a>

				<a class="gray_text info_element inline_element inline" href="#">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACAElEQVRoge2XMXKrMBCGdQQOEDJqrJqJC1NSEsbcwZ3lq0CRG0QdlKiCMjPmABwBbmDfYF8BxHgSLCkyefJ7+mf+GTX+dv8VLGOErKysrKysBIIcY8hIATlpISdtf8b4Ifg9fHWCnMCVs9XpHkWW5iPICf8Cv5ibzkeQr843CrSm828XyEhnOv9feIQw/n5Kq/PdXuIF+ZMihENGOshI10/tzmt0Qf5jy6NPwQt12frw3KwPzzC4eaEu8+hT8N1voAoxlNsCqriFKm77c4jvxZdrfOc464NbTKAzdgtv5zjXzccnqGK4chmfpiF+yldo/moiIjdjEahi/qX5i7kuX0pr6nIF+OekhgDnGwFaXb5cAGV4b2GActvp8n8jgPARWjyAjqAK8cwtnOc2kXEaQnAotx2U266/lb/YvMpeN44vu9dHUeYHlPlszzYNZT5Q5sNwZpT5gS7/BwHELyVCCO2Y5+zZphibnvOebYod8xxVvk4A4V4fmm9EzU9vZAwhw18uwLDX9+8bLtv89CZk+boBhFes2vxoWb5mAPFe1wvwC9+Npfe6cd8No5TWYZAcI5bUUZPWEaR1BEkdNckxYmkdBsby3z4CJ6lfixE656R+Ld4+ArU/HEvze/hlIuIiUaNSZGk+So8Rl4VPJ2UKH6nCR5vCf/wAVlZWVlb/jf4At3BHZPZDmRIAAAAASUVORK5CYII="
						class="info_element_icon">

					<div
						class="data_info_counter <?= floatFontSize($stat_data['active_users_count']) ?>"><?= $stat_data['active_users_count'] ?></div>
					<div class="data_info">пользователей</div>
				</a>


				<a class="gray_text info_element inline_element inline" href="/categories/">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABWUlEQVRoge3au07CUBjAcR7HByg+AINjy+omg03LYEoDsdDTBEJMIFCQDmwYtO4QnwAY5AHwPbgZHRg+Ji4DKi3E8/Xk+ydnbc4vvZ2TnFiMOlxGiY9M5XK8GRklPuI9p1AZsgSVVAIetSuopBJgyBLwnlOoDFmCbjYJ/dI1dLNJgnCPO+TWat5ozHvXmTc+ZaRNB3KFMljOA+QKZUibDoS5jma3hmq+fhEYolpu8a7UXtU6b4Bh5GvPX7rtzQJjVMst3lefvvvDD8AweoMJ2A3/UwuKwQbZYFjjdRkIgxGyw/hLnbWmR2GwQvYes8VRGMyQvTsz/xODHbK7My9z3f4FEwXIFuP6sx8xBPl3RMQfLSFediE+v0L8EHuDCbBmxJcoJy0ahVjGC7OxOlfct7rniiDYIgi2CIItgmCLINgiCLYIgi2CYIsg2CIItgiCLWEO1QhzzIlXa+WbL4URk8u/AAAAAElFTkSuQmCC"
						class="info_element_icon">

					<div class="data_info_counter"><?= $stat_data['category_count'] ?></div>
					<div class="data_info">Категорий</div>
				</a>


				<a class="gray_text info_element inline_element inline" href="#">
					<img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAACP0lEQVRoge2VP2vbQByGD7pk6NLiox4SKgf5/BUy5ht0aaF0iFSwjKEZMqbQC4IQSq20oU4rfwDTnKgHD/4AUqA0a6B/6Bo8BCoSY1TSIcOvQ6QS7NSyT7J0onrhBS967n1OAiOUJ0+escjNi8Vl02Ml03NLpgfcbXn9kvmrLTcvFhMdH3n4eAdSayAlIuDffJzjoWR6sGx6LCmBb/MQuP/mJ2DN4Wm/UHPad6v2dJ/hPMYH5RQIOijWbSnLAoA1J/wzFFmgoDlfMy2ANQdygVwgqwIR/gfEEFjaOcmugPRuCPfWj7InIL09g6Wdk9jGcwtI74d66IPXgjWnE9fgyAKzjkcIoWLdlrDmeKkL8IwfkehgzXFTEYgyPo4UqrbOLSCZw40ENobmJonQh0QZH2RUIu09XLkuwQUAS14Bi3SBkT4wcgoW6YIlr8Q1cBo+rh3ucQkAK9eBkUtgBEZ6CQdkM/L4Gfi4drg3G9yqPABGTm+ABz0HJj/kHj9nPgJW6UyAX9UiXVH5CBg5Dj2Alb+IykfAyPcpbugHfES3ROQjYKQXfkOkxwVPgI/gA3kMrHI+Af4bDogiKv/qEFZ+8e8DKtuR4Anw/UPkVf91u357wOTVWOAJ8POkFlXXF9Zoo6pQY1+lhq1Qw1Wo4fq/99doo6rq+oJw/Ee6flulr+oqNT6p1IBJVajxWdl6/ezJ5ss7QvCfPm9idWu3GQYe725L1RvFtPlIoUZ7dvjf22qnzUe88KBp87MvkCfPf5Y/xGGmfzissrMAAAAASUVORK5CYII="
						class="info_element_icon">

					<div
						class="data_info_counter <?= floatFontSize($stat_data['active_posts_count']) ?>"><?= $stat_data['active_posts_count'] ?></div>
					<div class="data_info">постов</div>
				</a>

			</div>

			<hr>


			<!--<a href="/register/" class="tagElement t7" style="margin: 20px auto;
		display: block; width: 250px; text-align: center">Регистрация</a> -->

			<button href="/register/" class="btn red" style="display: block; margin: 20px auto; height: 49px;">Регистрация</button>

		</div>
	</div>

	<!-- Наиболее популярные карты -->

	<div class="index_most_popular_wrapper">
		<div class="fwidth">
			<h1 style="font-size: 30px;font-size: 30px;color: #fff;text-align: left;">
				<span class="fa-fw fa fa-bar-chart"></span>&nbsp; Самые популярные карты
			</h1>
			<br>
			<hr>
			<br>

			<? foreach ($top_visited_maps as $map) {
				$map_data = $map['Map'];
				$img_path = site_url() . "/" . "files" . "/" . "map_files" . "/" . "m" . $map_data["id"] . "/";
				$def_img_path = site_url() . "/img/design/";
				$real_foto = Configure::read('MAP_FILE_UPLOAD_DIR') . DS . "m" . $map_data["id"] . DS . $map_data['map_image'];

				if ((!empty($map_data['map_image'])) AND (file_exists($real_foto))) {
					$image = $img_path . $map_data['map_image'];
				} else {
					$image = $def_img_path . "default_user_icon.png";
				}

				?>

				<div class="index_map_popular_block" style="background-image: url(<?php echo $image ?>);
					background-size: cover;
					background-image: linear-gradient(to bottom, rgba(255,255,255,0.7) 0%,rgba(255,255,255,0.7) 100%), url(<?php echo $image ?>);">

					<a class="user_prop_link popular_link_name"
					   href="/map/<?= $map_data['login'] ?>"><?= $map_data['name'] ?></a>
					<br>

					<a class="map_popular_avatar" href="/map/<?= $map_data['login'] ?>"
					   style="background-image: url(<?php echo $image ?>);">
					</a>

					<div class="map_popular_info_block">

						<?
						$show_map_description = false;
						if ($show_map_description) {
							?>
							<span class="popular_map_prop_text"><?= text_preview($map_data['text'], 100) ?></span>
							<span class="popular_map_prop_text">Пользователей: <?= $map[0]['entity_count'] ?></span>
						<? } ?>

						<span class="map_popular_element">
						<span class="map_user_count">
							<span class="fa fa-users fa-fw map_info_icon" aria-hidden="true"></span>
							<b><? echo $map[0]['entity_count'] ?></b>
						</span>
					</span>

						<span class="map_popular_element">
						<span class="map_user_count">
							<span class="fa fa-map-marker fa-fw map_info_icon" aria-hidden="true"></span>
							<b><?= $map['ItemsCount'] ?></b>
						</span>
					</span>

						<br>

						<? $show_map_users = true;
						if ($show_map_users) {
							foreach ($map['MapSomeUsers'] as $map_user) {


								$USER = $map_user['User'];
								$img_path = site_url() . "/" . "files" . "/" . "user_files" . "/" . "u" . $USER["id"] . "/";
								$def_img_path = site_url() . "/img/design/";
								$real_foto = Configure::read('USER_FILE_UPLOAD_DIR') . DS . "u" . $USER["id"] . DS . $USER['main_foto'];

								if ((!empty($USER['main_foto'])) AND (file_exists($real_foto))) {
									$image = $img_path . $USER['main_foto'];
								} else {
									$image = $def_img_path . "default_user_icon.png";
								} ?>
								<a class="micro_user_avatar" href="/user/<?= $USER['login'] ?>"
								   style="background-image: url(<?php echo $image ?>); margin-right: -3px">
								</a>


							<? }
						} ?>
					</div>

					<hr>

					<div class="map_popular_context">

					<span class="tagCloudHolder">
						<?
						$map_categories = isset($map['Categories']) ? $map['Categories'] : null;
						if ($map_categories != null) {
							foreach ($map_categories as $category) {
								?>
								<a href="<? echo site_url() . 'category/' . $category['Categories']['url_prefix'] ?>"
								   class="tagElement t2"><? echo $category['Categories']['name_ru'] ?></a>
							<? }
						} else { ?>
							<span class="dark_gray mini_text">Категории не указаны</span>
						<? } ?>
					</span>

						<hr>

						<span class="map_additional_text">
						<?
						if (isset($map['Hashtags']) AND count($map['Hashtags']) > 0) {
							?>
							<span class="invite_hashtag_holder">
								<? foreach ($map['Hashtags'] as $map_hashtag) {
									?>
									<a href="/search/?hashtag=<? echo $map_hashtag['Hashtag']['name_ru'] ?>"
									   class="popular_hashtag_link">#<? echo $map_hashtag['Hashtag']['name_ru'] ?>
									</a>

									<?
								} ?>
							</span>
							<?
						} else { ?>
							<span class="dark_gray mini_text">Хештеги не указаны</span>
						<? } ?>

					</span>


					</div>

				</div>
			<? } ?>


			<a href="/popular/" class="tagElement t7 no_template_map_button">Другие популярные карты</a>

		</div>
	</div>

	<!-- Шаблоны карт -->

	<script>

		$(document).scroll(function () {
			var TGCtop = $(".tag_cloud_wrapper").position().top * 1;
			var cur_scroll = $(document).scrollTop();
			var doc_height = $(document).height() * 1;
			var window_height = $(window).height() * 1;

			var templates_count = $(".template_link_item").length;
			var templates_height = $(".tag_cloud_wrapper").height() * 1.8;
			var shift_step = Math.floor(templates_height / templates_count);
			var last_item = 0;
			var item = Math.floor((cur_scroll - TGCtop + window_height - 300) / shift_step);
			if (last_item != item && cur_scroll - TGCtop + window_height - 300 > 0) {
				$(".template_link_item").removeClass('highlite_template');
				$(".template_link_item").eq(item - 1).addClass('highlite_template');
			}

		});

	</script>


	<div class="tag_cloud_wrapper" style="">
		<div class="fwidth">
			<h1 style="font-size: 30px;font-size: 30px;
color: #fff;
text-align: left;">
				<span class="fa-fw fa fa-bar-chart"></span>&nbsp;Популярные шаблоны карты
			</h1>
			<br>
			<hr>

			<div class="info_block_description" style="color: #fff">
				<div class="info_block_element">
					Шаблон карты позволяет создать нужную вам карту максимально быстро и с минимальными усилиями
				</div>
				<div class="info_block_element">
					Созданная по шаблону карта позволит задать нужные настройки для карты: настройки меток и постов,
					настройки приватности

				</div>
				<div class="info_block_element">
					Созданная по шаблону карта автоматически отметится в соответствующих тематике шаблона категориях.
				</div>
			</div>
			<hr>
			<br>

			<div class="tagCloudHolder">

				<? if ($map_templates != null) {
					foreach ($map_templates as $template) {
						?>
						<a href="<? echo site_url() . 'create_map/' . $template['TemplateTag']['id'] ?>"
						   class="template_link_item tagElement t<? echo $template['TemplateTag']['sorter'] ?>"><? echo $template['TemplateTag']['name'] ?></a>
					<? }
				} ?>

				<!--

				<div class="tagElement t7">Сходить на свидание</div>
				<div class="tagElement t6">Ищу девушку</div>
				<div class="tagElement t6">Приглашу в гости</div>
				<div class="tagElement t5">Знакомства в Москве</div>
				<div class="tagElement t5">Знакомства в Новосибирске</div>
				<div class="tagElement t5">Знакомства в Питере</div>
				<div class="tagElement t5">Знакомства в Сочи</div>
				<div class="tagElement t4">Найти земляков в городе</div>
				<div class="tagElement t4">Найти компанию</div>
				<div class="tagElement t4">Найти друзей</div>
				<div class="tagElement t4">Сходить в ночной клуб</div>
				<div class="tagElement t3">Ищу партнера для занятий танцами</div>
				<div class="tagElement t3">Найти партнера для занятий танцами</div>
				<div class="tagElement t2">Сходить в кино</div>
				<div class="tagElement t3">Сходить в театр</div>
				<div class="tagElement t3">Сходить в Эрмитаж</div>
				<div class="tagElement t3">Знакомства в слепую</div>
				<div class="tagElement t2">Организовать флешмоб</div>
				<div class="tagElement t2">Бесплатно сходить в кафе</div>
				<div class="tagElement t2">Вписаться на выходные</div>
				<div class="tagElement t2">Пригласить в гости</div>
				<div class="tagElement t2">Пригласить на тренинг</div>
				<div class="tagElement t2">Найти единомышленников</div>
				<div class="tagElement t2">Организовать конференцию</div>
				<div class="tagElement t2">Сходить поиграть в боулинг</div>
				<div class="tagElement t2">Сходить поиграть в бильярд</div>

				<div class="tagElement t3">Найти партнера для тренировок</div>

				<div class="tagElement t2">Погулять в парке</div>
				<div class="tagElement t2">Выехать на море</div>
				<div class="tagElement t2">Найти попутчика</div>
				<div class="tagElement t2">Попутчик в Таиланд</div>
				<div class="tagElement t2">Поиграть в мафию</div>
				-->
			</div>

			<a href="/create_map/" class="tagElement t7 no_template_map_button">Создать карту без
				шаблона</a>
		</div>
	</div>

	<!-- ОТзывы пользователей карт -->

	<div class="feedback_wrapper">
		<div class="fwidth">
			<h1 style="font-size: 30px;
color: #4389A1;
text-align: left;">
				<span class="fa-fw fa fa-comment"></span>&nbsp;Отзывы о проекте
			</h1>
			<br>
			<hr>
			<br>

			<div class="feed">
				<div class="normal_user_avatar"
				     style="background-image: url('<?= site_url() ?>img/d/feed1.jpg');"></div>
				<div class="feed_content">
					Нашему туристическому проекту "Путешествуем Вместе" давно требовалось что-то вроде своей управляемой
					карты с возможностью добавления фотоальбомов и отчетов о путешествиях и походах,
					теперь у нас есть геосеть Моя Карта, мы нашли то, что искали.
				</div>
			</div>

			<div class="feed">

				<div class="feed_content">
					Нашему агентству не хватало до последнего времени удобной карты с гибкой возможностью работы с ней,
					не прибегая к услугам программиста, чтобы показывать на ней наши
					маршруты путешествий, горячие предложения туров, отзывы наших клиентов и прочую полезную информацию.
					Такую возможность мы нашли на проекте Моя Карта. Спасибо им. Антон Юрченко, руководитель
					турагентства "За туром".

				</div>
				<div class="normal_user_avatar"
				     style="background-image: url('<?= site_url() ?>img/d/feed2.jpg');"></div>
			</div>

			<div class="feed">
				<div class="normal_user_avatar"
				     style="background-image: url('<?= site_url() ?>img/d/feed3.jpg');"></div>
				<div class="feed_content">
					"Мы с сестрой открыли на проекте Моя Карта сообщество быстрых знакомств в нашем городе, и к нам
					постоянно приходят люди из других социальных сетей и просто знакомые,
					тысячи новых постов и меток каждую неделю, даже не представляли что карта будет такой посещаемой и
					востребованной. Молодцы, Моя Карта!!! :)" Полина Sweet Щербакова
				</div>
			</div>

			<div class="feed">

				<div class="feed_content">
					"Мы перенесли все наши рыболовные места на карту и создали единую рыбаловецкую карту Саратовской
					области, очень удобно и наглядно, теперь можно планировать
					подходящий маршрут, смотреть кто где собирается рыбачить и рыбачил, смотреть фотоальбомы с мест
					рыбалки, а также делать прогнозы на грядущие поездки."
					Виктор Громашев, Саратовский Рыбловный клуб.
				</div>
				<div class="normal_user_avatar"
				     style="background-image: url('<?= site_url() ?>img/d/feed4.jpg');"></div>
			</div>


			<div class="feed">
				<div class="normal_user_avatar"
				     style="background-image: url('<?= site_url() ?>img/d/feed5.jpg');"></div>
				<div class="feed_content">
					"Я в геосоциальной сети Моя Карта открыла карту общества собаководов Санкт-Петербурга и веду ее.
					Теперь у нас есть возможность планировать встречи, узнавать кто где живет,
					давать обьявления на карте, искать поблизости знакомых. Очень удобно!" Мария, карта общества
					собаководов г. Санкт-Петербург.
				</div>

			</div>

			<div class="indexCategoryHolder">
				<? /*
                foreach ($main_categories as $category) {
                    ?>
                    <div style="width: 320px; display: inline-block;">
                        <img width=30px style="display:inline; vertical-align: middle"
                             src="<? echo $category['Icon']['icon_string'] ?>"/>
                        <a href="<? echo site_url() . 'category/' . $category['Categories']['url_prefix'] ?>"
                           class="tagElement t2"
                           style="display:inline;  vertical-align: middle"><? echo $category['Categories']['name_ru'] ?></a>
                    </div>
                <? }*/ ?>
			</div>

		</div>

	</div>

	<?
	echo $this->element('footer_main');
	?>

</body>
</html>
