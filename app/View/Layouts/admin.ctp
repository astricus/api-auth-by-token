<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Административная панель
		<?php if (isset($title)) echo $title; ?>
	</title>
	<?php
	echo $this->Html->meta('icon');
	echo $this->Html->css('old_main');
	echo $this->Html->css('jquery-ui');

	echo $this->Html->script('jquery');
	echo $this->Html->script('jquery-ui.min');
	echo $this->Html->script('datepicker');
	echo $this->Html->script('admin_geo');
	echo $this->Html->script('admin_service');

	echo $this->Html->script('admin_message_type');
	echo $this->Html->script('admin_action_type');
	echo $this->Html->script('filter_country_city');
	echo $this->Html->script('filter_category_service');
	echo $this->Html->script('hint');
	echo $this->Html->script('admin_action_type');
	//echo $this->Html->script('admin_tender');
	echo $this->Html->script('old_main');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>

<div id="container">

	<? echo $this->element('getup'); ?>

	<div id="header">
		<h1>Административная панель <?php echo Configure::read('SITENAME'); ?></h1>
		<?php if ($this->request->params['action'] !== "auth") { ?>

			<? if ($admin_type == 1) { ?>

				<div class="green_element aligned_left">

					<span class="small_text admin_nav_header">Администрирование системы: </span>

					<?php echo $this->Html->link("Главная", array('controller' => 'admin', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Настройки", array('controller' => 'admin/setting', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Действия в системе", array('controller' => 'admin/actiontype', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Локализация", array('controller' => 'admin/geo', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Администраторы", array('controller' => 'admin/control', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("IP и безопасность", array('controller' => 'admin/restrict', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Курсы валют", array('controller' => 'admin/currency', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>

				</div>
			<? } ?>

			<div class="green_element aligned_left">

				<span class="small_text admin_nav_header">Материалы и страницы: </span>

				<?php echo $this->Html->link("Блог", array('controller' => 'admin/blog', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
				<?php echo $this->Html->link("Новости", array('controller' => 'admin/news', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
				<?php echo $this->Html->link("Материалы", array('controller' => 'admin/content', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
				<?php echo $this->Html->link("Вакансии", array('controller' => 'admin/offer', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
				<?php echo $this->Html->link("Вопросы-ответы", array('controller' => 'admin/faq', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>

			</div>

			<? if ($admin_type < 3) { ?>

				<div class="green_element aligned_left">
					<span class="small_text admin_nav_header">Пользователи, мастера, услуги: </span>

					<?php echo $this->Html->link("Мастера", array('controller' => 'admin/user', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Услуги", array('controller' => 'admin/service', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Заявки", array('controller' => 'admin/order', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Финансы/Платежи", array('controller' => 'admin/payment', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Поддержка", array('controller' => 'admin/ticket', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Отзывы", array('controller' => 'admin/feed', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Файлы", array('controller' => 'admin/file', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Горячие предложения", array('controller' => 'admin/specaction', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Платежные кошельки", array('controller' => 'admin/userwallet', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Соискатели", array('controller' => 'admin/resume', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Специальности", array('controller' => 'admin/position', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>
					<?php echo $this->Html->link("Типы организаций", array('controller' => 'admin/CompanyType', 'action' => 'index'), array('class' => 'admin_nav_link link_violet link_colored_button')); ?>

				</div>

			<? } ?>

			<div class="green_element aligned_left">

				<table width="100%">
					<tr>
						<td width="900px">
							<span class="small_text admin_nav_header">Дополнительно: </span>
							<?php echo $this->Html->link("Профиль", array('controller' => 'admin/profile', 'action' => 'index'), array('class' => 'admin_nav_link link_blue link_colored_button')); ?>
							<?php echo $this->Html->link("Статистика", array('controller' => 'admin/statistic', 'action' => 'index'), array('class' => 'admin_nav_link link_blue link_colored_button')); ?>
							<?php echo $this->Html->link("Финансовая Статистика", array('controller' => 'admin/paystatistic', 'action' => 'index'), array('class' => 'admin_nav_link link_blue link_colored_button')); ?>
							<?php echo $this->Html->link("Комментарии", array('controller' => 'admin/comment', 'action' => 'index'), array('class' => 'admin_nav_link link_blue link_colored_button')); ?>
							<?php echo $this->Html->link("Рассылки по почте", array('controller' => 'admin/mailing', 'action' => 'index'), array('class' => 'admin_nav_link link_blue link_colored_button')); ?>

						<td align="right">
							<?php echo $this->Html->link("Выход", array('controller' => 'admin', 'action' => 'logout'), array('class' => 'admin_nav_link link_red link_colored_button')); ?>
						</td>
					</tr>
				</table>

			</div>

		<? } ?>
	</div>
	<div id="content">

		<div class="subcontent">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
</div>
</body>
</html>