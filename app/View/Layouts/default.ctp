<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        Cтраница не найдена!
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="w1-verification" content="170404989232"/>
    <?php
    echo $this->Html->meta('icon'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <?

    echo $this->Html->css('css/font-awesome.min');
    echo $this->Html->css('jquery-ui');
    //echo $this->Html->css('style');
    echo $this->Html->css('new_main');
    echo $this->Html->script('jquery-ui.min');
    //echo $this->Html->script('main');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
 ?>

    <script>

        <?php if ($this->Session->check('User')) {?>
        var USER_ID = '<?=$user_id?>';
        <?}?>

    </script>
</head>
<body>


<?php if ($this->Session->check('User')) { ?>
    <div class="bo_to_site_switch">
        <img
                src="<?php echo site_url() . Configure::read('DESIGN_IMAGE_DIR_RELATIVE') . Configure::read('ADMIN_ICON_FILE'); ?>"
                class="bo_icon">
        <?php echo $this->Html->link(L('GO_TO_BO'), array('controller' => 'backoffice', 'action' => 'index'), array('class' => 'main_nav_link  link_colored_button header_link_to_box')); ?>
    </div>
<?php } ?>


<header>
    <div class="fwidth">
        <a href="<?php echo site_url() ?>" class="logo">
            TEST REST API AUTH
        </a>
    </div>

</header>

<div class="content">
    <div class="main_content">

        <?
        ?>

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>

    </div>
</div>

</body>
</html>