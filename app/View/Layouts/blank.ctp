<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo Configure::read('SITENAME'); ?> <?php echo $page_title; ?>
	</title>
	<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('old_main');
	echo $this->Html->script('jquery');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>
<div id="container">
	<div id="header">
		<div id="header_content">
			<h1><?php echo L('BO_ON_PROJECT') . " " . Configure::read('SITENAME'); ?></h1>
		</div>
	</div>
	<div id="content">
		<?php echo $this->element('auth_form', array('backoffice' => true, 'auth_error' => $auth_error, 'auth_error_text' => $auth_error_text)); ?>

		<?php echo $this->Session->flash(); ?>

		<?php echo $this->fetch('content'); ?>
	</div>
	<div id="footer">
		<div id="footer_content">
			<p>
				Проект <?php echo Configure::read('SITENAME'); ?>
			</p>
		</div>
	</div>
</div>
</body>
</html>
