<!-- Форма авторизации -->
<div class="auth_form">

    <form action="/auth" method="post" name="login_form">

        <div class="auth_form_element">
            <input type="text" name="login" placeholder="login">
        </div>
        <br>

        <div class="auth_form_element">
            <input type="password" name="pass" size="20">
        </div>
        <br>

        <div class="auth_form_element">
            <input type="submit"  value="Enter">
        </div>

        <?php if (isset($auth_error)) { ?>
            <div class="auth_error">
                <div class="error_icon">!</div> <?php echo $auth_error_text; ?></div>
        <? } ?>

    </form>
</div>