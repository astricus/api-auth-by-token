<?php if (!$this->Session->check('User')) { ?>

	<!-- Форма авторизации -->
	<div class="auth_form">

		<form action="<?php echo site_url(); ?>/login/login" method="post" name="login_form">

			<?php if (isset($backoffice)) { ?>
				<input type="hidden" name="data[User][backoffice]" value="1">
			<? } ?>

			<div class="auth_form_element">
				<input type="text" name="data[User][mail]" class="login_form_element" size="20" placeholder="Email">
			</div>
			<br>

			<div class="auth_form_element">
				<input type="password" name="data[User][password]" class="login_form_element" size="20"
				       placeholder="<?php echo L('PASSWORD') ?>">
			</div>
			<br>

			<div class="auth_form_element">
				<input type="submit" class="login_form_element" value="<?php echo L('ENTER') ?>">
			</div>

			<?php if ($auth_error) { ?>
				<div class="auth_error">
					<div class="error_icon">!</div> <?php echo $auth_error_text; ?></div>
			<? } ?>


		</form>
	</div>

	<div class="register_login">
		<a class="reg_link white_text"
		   href="<?php echo site_url() . "/recovery/" ?>"><?php echo L('FORGOT_PASSWORD') ?></a>
		<a class="reg_link white_text" href="<?php echo site_url() . "/register" ?>"><?php echo L('REGISTRATION') ?></a>
	</div>

<?php } ?>