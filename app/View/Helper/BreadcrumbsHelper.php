<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class BreadcrumbsHelper extends Helper
{
	var $helpers = array('Html');

	public $settings;
	public $url_controller;
	public $url_controller_name;

	public function __construct(View $view, $settings)
	{

		$this->settings = $settings;
		$this->url_controller = $settings['0']['url_controller'];
		$this->url_controller_name = $settings[0]['url_controller_name'];
		$this->url_method = $settings[1]['url_method'];
		$this->url_method_name = $settings[1]['url_method_name'];
		//parent::__construct($url_controller, $url_method);
	}

	function print_link()
	{
		// Используем HTML хелпер для вывода
		// оформатированных данных:

		$navigate = $this->Html->link($this->url_controller_name, array('controller' => $this->url_controller, 'action' => "/"), array('class' => 'top_navigate')) .
			$this->Html->link($this->url_method_name, array('controller' => $this->url_controller, 'action' => $this->url_method), array('class' => 'top_navigate'));

		return $this->output("<div class=\"editOuter\">$navigate</div>");
	}
}
