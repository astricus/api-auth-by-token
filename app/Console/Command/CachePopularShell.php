<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.05.17
 * Time: 16:39
 */

App::uses('ComponentCollection', 'Controller');
App::uses('MapController', 'Controller');
App::uses('MapComponent', 'Controller/Component');
App::uses('View', 'View');

//популярные карты

class CachePopularShell extends AppShell
{

	public $uses = array(
	);

	public function main()
	{
		$this->out(__CLASS__ . ' init');
		$this->job();
	}

	public function job()
	{
		$page_max = 50;
		$collection = new ComponentCollection();
		$this->MapComComponent = $collection->load('MapCom');
		$this->CachePageComponent = $collection->load('CachePage');

		for ($page = 1; $page < $page_max; $page++) {
			$raw_html = null;
			$popular_maps = $this->MapComComponent->get_popular_maps($page);
			if(count($popular_maps)>0) {
				$view = new View();
				$view->set('top_visited_maps', $popular_maps);
				$raw_html = $view->render("Map/popular_content", false);
				$hash = $this->CachePageComponent->cache_hash($page);
				$this->CachePageComponent->set_cache_file($hash, $raw_html);
				$this->out($page . " / " . $hash . " created ");
			} else {
				break;
			}
		}
		exit;
	}

}