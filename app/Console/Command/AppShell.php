<?php

App::uses('ComponentCollection', 'Controller');
App::uses('TaskComponent', 'Controller/Component');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class AppShell extends Shell
{
	public $uses = array();

	public $components = array('TaskComponent');

	public function main()
	{
		$this->out(__CLASS__ . ' init');
		$this->job();
	}

	public function job()
	{
		$collection = new ComponentCollection();
		$this->TaskComponent = $collection->load('Task');

		$time_start = microtime_float();

		while (true) {
			//метка последнего процесса
			if (microtime_float() - $time_start > 1) {
				$active_system_tasks = $this->TaskComponent->system_task_list();
				//цикл по типам задач и запуск готовых к запуску
				foreach ($active_system_tasks as $task) {

					/*
					 *  если задача запускалась давно (прошел интервал простоя или наступило время запуска и задача сейчас не запущена) - запускаем ее
					 * */
					if ($this->TaskComponent->is_task_ready_for_launch($task)) {
						if ($this->TaskComponent->prepare_to_launch($task['SystemTask']['id'], $task['SystemTask']['params'])) {
							$this->out('TASK PREPARED: ' . $task['SystemTask']['id']);
						}
					}
				}

				//цикл по конкретным задачам, ожидающим запуска
				$ready_tasks = $this->TaskComponent->standby_task_list();
				foreach ($ready_tasks as $task) {
					/*
					 *  если задача запускалась давно (прошел интервал простоя или наступило время запуска и задача сейчас не запущена) - запускаем ее
					 * */
						$this->out('found standby task: ' . $task['SystemTask']['id']);

						$this->out('TASK: ' . $task['SystemTask']['id'] . " started");
						$action = "JOB_" . $task['SystemTask']['job'];
						if (method_exists($this->TaskComponent, $action)) {

							$task_id = $this->TaskComponent->start_task($task, null);
							$params = unserialize($task['Task']['params']);
							$this->TaskComponent->$action($task, $task_id, $params);

							$this->out('TASK started');
						} else {
							$this->out('TASK not found ');
						}

					$this->out('ID: ' . $task['SystemTask']['id']);
				}

				// цикл по задачам и остановка "зависших" запущенных
				//
				$started_tasks = $this->TaskComponent->launched_task_list();
				//цикл по типам задач и запуск готовых к запуску
				foreach ($started_tasks as $task) {

					/*
					 *  если у задачи истек MAX_EXECUTION_TIME - отключаем ее
					 * */
					if ($this->TaskComponent->is_task_expired($task)) {
						$this->out('task expired: ' . $task['SystemTask']['id']);
						$this->TaskComponent->force_stop($task, $task['Task']['id']);
						$this->out('TASK stopped');

					}
					$this->out('ID: ' . $task['SystemTask']['id']);
				}

				$time_start = microtime_float();
				exec("chown -R www-data:www-data " . APP . "/tmp");
			}
		}
	}
}